#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <map>
using namespace std;

typedef int Value;
typedef int Index;

int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */ 
    std::vector<std::pair<int,int>> solutions;
    int T;
    cin >> T;
    while(T>=0){
        int M;    
        cin >> M;
        int N;
        cin >> N;
        bool insert = true;
        std::map<Value,Index> sumOfValues;
        for(int i = 0; i < N; ++i) {
            int value;
            cin >> value;
            if(insert == false) continue;
            auto found = sumOfValues.find(value);
            if(found != sumOfValues.end()){
                // Print the index
                solutions.push_back(std::make_pair(found->second + 1,i + 1));
                insert = false;
            }else{
                sumOfValues[M - value] = i;
            }
        }
        --T;
    }
    for(auto& i: solutions){
        std::cout << i.first << " " << i.second << '\n';
    }
    return 0;
}
