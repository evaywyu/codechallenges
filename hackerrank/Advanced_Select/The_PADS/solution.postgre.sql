-- postgres unions will automatically order all items by a single order by
-- therefore, we need to add another column to order by after the union. 
-- to make it look a table was appended.
SELECT
	descriptions
FROM
(
	(SELECT
		0 tableorder,
		name || '(' || LEFT(occupation, 1) || ')' as descriptions
	FROM Occupations)
	UNION
	(SELECT
		1 tableorder,
		'There are a total of ' || COUNT(*) || ' ' || LOWER(occupation) || 's.' as descriptions
	FROM Occupations
	GROUP BY occupation)
	ORDER BY tableorder, descriptions
) ordered_by_tables
