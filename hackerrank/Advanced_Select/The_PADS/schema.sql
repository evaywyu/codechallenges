-- Occupations
CREATE TABLE occupations
(
	name varchar(60),
	occupation varchar(60)
);

INSERT INTO occupations 
VALUES ('Samantha','Doctor'),
		('Julia','Actor'),
		('Maria','Actor'),
		('Meera','Singer'),
		('Ashley','Professor'),
		('Ketty','Professor'),
		('Christeen','Professor'),
		('Jane','Actor'),
		('Jenny','Doctor'),
		('Priya','Singer');
