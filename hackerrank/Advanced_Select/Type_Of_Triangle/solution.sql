-- Note: the 'Not A Triangle' must be first statement 
-- to eliminate any phony triangles before comparing for 
-- triangle type
SELECT 
    CASE
        WHEN a+b <= c or b+c <= a or a+c <= b THEN 'Not A Triangle'
		WHEN a=b and b=c THEN 'Equilateral'
        WHEN a=b or b=c or a=c THEN 'Isosceles'
		ELSE 'Scalene'
    END
from triangles
