-- Outer Subquery selects all the values 
-- when you group values by rownumber
SELECT 
	MAX(CASE WHEN Occupation = 'Doctor' THEN Name ELSE NULL END) AS Doctors,
	MAX(CASE WHEN Occupation = 'Professor' THEN Name ELSE NULL END) AS Professors,
	MAX(CASE WHEN Occupation = 'Singer' THEN Name ELSE NULL END) AS Singers,
	MAX(CASE WHEN Occupation = 'Actor' THEN Name ELSE NULL END) AS Actors
-- Inner Subquery produces the table with an extra column 
-- that is the index on a rows per occupation and sorts it by name
FROM ( SELECT 
			Name,
			Occupation,
			ROW_NUMBER() OVER (PARTITION BY Occupation ORDER BY name ASC) rownum
		FROM occupations
		ORDER BY rownum
) AS row_numbers
GROUP BY rownum

-- Same Solution with CTE instead for legibility:
WITH numbered_by_occupation as
(SELECT 
	Name,
	Occupation,
	ROW_NUMBER() OVER (PARTITION BY Occupation ORDER BY name ASC) rownum
 FROM occupations
 ORDER BY rownum
)
SELECT 
	MAX(CASE WHEN Occupation = 'Doctor' THEN Name ELSE NULL END) AS Doctors,
	MAX(CASE WHEN Occupation = 'Professor' THEN Name ELSE NULL END) AS Professors,
	MAX(CASE WHEN Occupation = 'Singer' THEN Name ELSE NULL END) AS Singers,
	MAX(CASE WHEN Occupation = 'Actor' THEN Name ELSE NULL END) AS Actors
FROM numbered_by_occupation
GROUP BY rownum

-- Final Long method is to outer join. 
-- Note, there is no outer join in MySQL, 
-- tested in Postgres
