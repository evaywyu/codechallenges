/**
Program to traverse a binary tree
in different orders using  a stack
*/
#include <stack>
#include <queue>
#include <iostream>

class TreeNode {
public:
  explicit TreeNode(int value)
    : val(value),
    left(nullptr),
    right(nullptr),
    visited(false)
  {}
  ~TreeNode() {
    delete left;
    delete right;
  }
  int val;
  bool visited;
  TreeNode* left;
  TreeNode* right;
};

void print_pre_order(TreeNode* root) {
  if (root == nullptr) return;

  std::stack<TreeNode*> s;
  s.push(root);
  while (!s.empty()) {
    TreeNode* actualNode = s.top();
    s.pop();
    // Print the actual value of the node
    std::cout << actualNode->val << " ";
    if (actualNode->right != nullptr) {
      s.push(actualNode->right);
    }
    if (actualNode->left != nullptr) {
      s.push(actualNode->left);
    }
  }
  std::cout << '\n';
}

void print_post_order(TreeNode* root) {
  if (root == nullptr) return;
  std::stack<TreeNode*> s;
  s.push(root);
  while (!s.empty()) {
    TreeNode* actualNode = s.top();
    if (actualNode->left == nullptr && actualNode->right == nullptr) {
      actualNode->visited = true;
      std::cout << actualNode->val << " ";
      s.pop();
    }
    else {
      if (actualNode->left != nullptr && !actualNode->left->visited) {
        actualNode->visited = true;
        s.push(actualNode->left);
      }
      else if (actualNode->right != nullptr && !actualNode->right->visited) {
        s.push(actualNode->right);
      }
      else {
        std::cout << actualNode->val << " ";
        actualNode->visited = true;
        s.pop();
      }
    }
  }
  std::cout << '\n';
}

void print_post_order_no_visited_additional_pointer(TreeNode* root){
  if(root == nullptr){
    return;
  }
  std::stack<TreeNode*> s;
  s.push(root);
  TreeNode* previousNode = nullptr;
  while(!s.empty()) {
    TreeNode* temp = s.top();
    if(!previousNode || previousNode == temp || previousNode == temp->right) {
      if(temp->left != nullptr) {
        s.push(temp->left);
      }else if(temp->right != nullptr) {
        s.push(temp->right);
      }else if(temp->left == previousNode){
        if(temp->right != nullptr) {
          s.push(temp->right);
        }else {
          std::cout << temp->val << " ";
          s.pop();
        }
        previousNode = temp;
      }
    }
  }
  std::cout << "\n";
}

void print_post_order_no_visited_two_stack(TreeNode* root) {
  if(root == nullptr) {
    return;
  }
  std::stack<TreeNode*> node1;
  std::stack<TreeNode*> node2;
  node1.push(root);
  while(!node1.empty()){
    TreeNode* tmp = node1.top();
    node1.pop();
    node2.push(tmp);
    if(tmp->left){node1.push(tmp->left);}
    if(tmp->right){node1.push(tmp->right);}
  }
  while(!node2.empty()){
    TreeNode* tmp = node2.top();
    node2.pop();
    std::cout << tmp->val << " ";
  }
  std::cout << "\n";
}

void print_in_order(TreeNode* root) {
  if (root == nullptr) return;
  std::stack<TreeNode*> s;
  TreeNode* actualNode = root;
  while (!s.empty() || actualNode != nullptr) {
    if (actualNode != nullptr) {
      s.push(actualNode);
      actualNode = actualNode->left;
    }
    else {
      actualNode = s.top();
      s.pop();
      std::cout << actualNode->val << " ";
      actualNode = actualNode->right;
    }
  }
  std::cout << '\n';
}

void print_level_order(TreeNode* root) {
  if (root == nullptr) return;

  std::queue<TreeNode*> s;
  s.push(root);
  while (!s.empty()) {
    TreeNode* actualNode = s.front();
    std::cout << actualNode->val << " ";
    if (actualNode->left != nullptr) {
      s.push(actualNode->left);
    }
    if (actualNode->right != nullptr) {
      s.push(actualNode->right);
    }
    s.pop();
  }
  std::cout << " ";
}


void rec_print_pre_order(TreeNode* root){
	if(root == nullptr)return;
	std::cout << root->val << " ";
	rec_print_pre_order(root->left);
	rec_print_pre_order(root->right);
}

void rec_print_in_order(TreeNode* root){
	if(root == nullptr) return;
	rec_print_in_order(root->left);
	std::cout << root->val << " ";
	rec_print_in_order(root->right);
}

void rec_print_post_order(TreeNode* root){
	if(root == nullptr) return;
	rec_print_post_order(root->left);
	rec_print_post_order(root->right);
	std::cout << root->val << " ";
}


int main(int argc, char** argv) {
  TreeNode* root = new TreeNode(1);
  root->left = new TreeNode(2);
  root->right = new TreeNode(3);
  root->left->left = new TreeNode(4);
  root->left->right = new TreeNode(5);

  print_pre_order(root);
  print_post_order(root);
  print_post_order_no_visited_two_stack(root);
  print_in_order(root);
  print_level_order(root);

  std::cout << "\nRecursivos:\n";
  rec_print_pre_order(root); std::cout << '\n';
  rec_print_post_order(root); std::cout << '\n';
  rec_print_in_order(root); std::cout << '\n';

  delete root;

  return 0;
}

