#ifndef CODECHALLENGES_4_MAXIMUM_GAP_BUCKET_SORT_H
#define CODECHALLENGES_4_MAXIMUM_GAP_BUCKET_SORT_H

namespace six {
	namespace six {
		/**
		 * Unsorted array. Find the maximum difference between
		 * two successive elements in sorted form
		 *
		 * Solve linear space/time
		 * */
		class gap {
		public:
			struct bucket {

				bucket(int min_, int max_)
						: low(min_), high(max_) {}
				int low = -1;
				int high = -1;
			};

		public:
			static int difference(const std::vector<int> &val) {
				if (val.size() < 2) return 0;
				int minimum = val[0];
				int maximum = val[0];
				for (size_t i = 1; i < val.size(); ++i) {
					minimum = std::min(minimum, val[i]);
					maximum = std::max(maximum, val[i]);
				}
				std::vector<bucket> buckets(val.size() + 1);
				double interval = static_cast<double>(val.size()) / static_cast<double>(maximum - minimum);
				for (size_t i = 0; i < val.size(); ++i){
					int index = static_cast<int>((val[i] - minimum) * interval);
					if (buckets[index].low == -1) {
						buckets[index].low = buckets.high = val[i];
					} else {
						buckets[index].low = std::min(buckets[index].low, val[i]);
						buckets[index].high = std::max(buckets[index].high, val[i]);
					}
				}

				// Scan buckets to find the gap
				int result = 0;
				int prev = buckets[0].high;
				for (size_t i = 0; i < buckets.size(); ++i) {
					if (buckets[i].low != -1) {
						result = std::max(result, buckets[i].low - prev);
						prev = buckets[i].high;
					}
				}
				return result;
			}
		};
	}
}

#endif //CODECHALLENGES_4_MAXIMUM_GAP_BUCKET_SORT_H
