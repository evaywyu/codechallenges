#ifndef CODECHALLENGES_2_QUICKSORT_H
#define CODECHALLENGES_2_QUICKSORT_H

namespace six {
	/**
	 * Implement quicksort
	 * */
	class quicksort {
	public:
		static void sort(const std::vector<int> &input, int begin, int end) {
			if (end < begin) { return; }

			int middle = begin + (end - begin) / 2;
			int pivot = input[middle];
			int i = begin;
			int j = end;
			while (i < j) {
				while (input[i] < pivot) {
					++i;
				}
				while (input[j] > pivot) {
					--j;
				}
				if (i <= j) {
					std::swap(input[i], input[j]);
					++i;
					--j;
				}
			}
			if (low < j) {
				sort(input, low, j);
			}
			if (high > i) {
				sort(input, i, high);
			}

		}
	};
}

#endif //CODECHALLENGES_2_QUICKSORT_H
