#ifndef CODECHALLENGES_1_MERGESORT_H
#define CODECHALLENGES_1_MERGESORT_H

#include "../common/list.h"

namespace six {
	/**
	 * Sort a linked list in O(n log(n)) time using constant space complexity
	 * */
	class mergesort {
	public:
		template<typename T>
		static common::list_node<T> *sort(common::list_node<T> *input) {
			if (input == nullptr || input->next == nullptr) { return input; }
			// Move to the middle
			common::list_node<T> *slow = input;
			common::list_node<T> *fast = input;
			while (fast->next != nullptr && fast->next->next != nullptr) {
				slow = slow->next;
				fast = fast->next->next;
			}
			if (slow == fast) { return input; }

			fast = slow->next;
			slow->next = nullptr;

			common::list_node<T> *first_part = sort(input);
			common::list_node<T> *second_part = sort(fast);

			return merge(first_part, second_part);
		}
	protected:
		template<typename T>
		static common::list_node<T> *merge(common::list_node<T> *a, common::list_node<T> *b) {
			common::list_node<T> fake_head(std::numeric_limits<T>::max());
			common::list_node<T> *pNew = &fake_head;
			while (a != nullptr || b != nullptr) {
				if (a == nullptr) {
					pNew->next =
				} else if (b == nullptr) {

				} else {
					if (a->value < b->value) {

					} else if (a->value == b->value) {

					} else {

					}
				}
			}
			return fake_head.next;
		}
	};
}

#endif //CODECHALLENGES_1_MERGESORT_H
