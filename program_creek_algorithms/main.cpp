#include "1_array_string/1_rotate_array_reverse_string.h"
#include "1_array_string/2_reverse_polish_notation.h"
#include "1_array_string/3_isomorphic_strings.h"

#include "2_matrix/2_spiral_matrix.h"
#include "3_list/1_add_two_numbers.h"
#include "3_list/2_reorder_list.h"


int main() {
  bool test_one = true;
  bool test_two = true;
  bool test_three = true;

  if (test_one) {
    one::rotate_array::test();
    one::rotate_words::test();
    one::polish_notation::test();
    one::isomorphic_strings::test();
  }

  if (test_two) {
    two::spiral_matrix::test();
    two::spiral_creator::test();
  }

  if (test_three) {
    three::add_two_numbers::test();
    three::reorder_list::test();
  }

  return 0;
}
