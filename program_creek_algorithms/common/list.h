#ifndef CODECHALLENGES_LIST_H
#define CODECHALLENGES_LIST_H

namespace common {
  template<typename T>
  class list_node {
  public:
    list_node() : value(), next(nullptr) {}

    list_node(const T &v) : value(v), next(nullptr) {}

    void print() {
      std::cout << value;
      list_node<T> *ptr = next;
      while (ptr != nullptr) {
        std::cout << '-';
        std::cout << ptr->value;
        ptr = ptr->next;
      }
      std::cout << '\n';
    }

    /**
     *
     * 1-2-3
     *
     * 3-2-1
     *
     * P      C
     * p
     * 1 ---> 2 ---> 3
     *
     * auto temp = curr->next
     *
     * P      C      t
     * p
     * 1 ---> 2 ---> 3
     *
     * curr->next = prev
     *
     * P      C     t
     * p
     * 1 ---> 2     3
     *   <---
     *
     * prev = curr
     *        P     t
     * p      C
     * 1 ---> 2     3
     *   <---
     *
     * curr = temp
     *
     *         P      t
     *  p             C
     *  1 ---> 2      3
     *    <---
     *
     *  auto temp = curr->next
     *
     *  p      P      C    t
     *  1 ---> 2      3    X
     *    <---
     *
     *  curr->next = prev
     *
     *  p      P      C    t
     *  1 ---> 2      3    X
     *    <---   <---
     *
     *  prev = curr
     *                P
     *                C     t
     *  1 ---> 2      3     X
     *    <---   <---
     *
     *  curr = temp
     *
     *                P     t
     *                      C
     *  1 ---> 2      3     X
     *    <---   <---
     *
     *  ptr->next = nullptr
     *
     *  p             P
     *  1 <--- 2 <--- 3
     * */
    static list_node<T>* reverse(list_node<T> *ptr) {
			if (ptr == nullptr || ptr->next == nullptr) {
        return ptr;
      }

      common::list_node<T> *curr = ptr->next;
      common::list_node<T> *prev = ptr;

      while (curr != nullptr) {
        auto temp = curr->next;
        curr->next = prev;
        prev = curr;
        curr = temp;
      }

      ptr->next = nullptr;

      return prev;
    }

    static void release(list_node<T> *ptr) {
      while (ptr != nullptr) {
        auto next = ptr->next;
        delete ptr;
        ptr = next;
      }
    }

    T value;
    list_node<T> *next;
  };

  template<typename T>
  class tree_node {
  public:

    T value;
    tree_node<T> *left = nullptr;
    tree_node<T> *right = nullptr;
  };
}

#endif //CODECHALLENGES_LIST_H
