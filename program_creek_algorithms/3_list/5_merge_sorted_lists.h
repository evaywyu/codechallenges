#ifndef PROGRAM_CREEK_ALGORITHMS_5_MERGE_SORTED_LISTS_H
#define PROGRAM_CREEK_ALGORITHMS_5_MERGE_SORTED_LISTS_H

#include "../common/list.h"

namespace three {
  /**
   * Merge two sorted linked lists and return it as a new list. The new list
   * should be made by splicing together the nodes of the first two lists.
   *
   * 1 3 5 7 9
   * 2 4 6 8 10
   *
   * 1 2 3 4 5 6 7 8 9 10
   * */
  class merge_sorted {
  public:
    template<typename T>
    static common::list_node<T> *merge(common::list_node<T> *a, common::list_node<T> *b) {
      common::list_node<T> head;
      common::list_node<T> *p = &head;

      while (a != nullptr || b != nullptr) {
        if (a == nullptr) {
          // next is b
          p->next = b;
          break;
        }
        if (b == nullptr) {
          // next is a
          p->next = a;
          break;
        }
        if (a->value < b->value) {
          // add a
          p->next = a;
          a = a->next;
        } else {
          // add b
          p->next = b;
          b = b->next;
        }
        p = p->next;
      }
      return head.next;
    }
  };
}

#endif //PROGRAM_CREEK_ALGORITHMS_5_MERGE_SORTED_LISTS_H
