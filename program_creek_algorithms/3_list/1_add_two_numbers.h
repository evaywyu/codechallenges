#ifndef PROGRAM_CREEK_ALGORITHMS_1_ADD_TWO_NUMBERS_H
#define PROGRAM_CREEK_ALGORITHMS_1_ADD_TWO_NUMBERS_H

#include "../common/list.h"

namespace three {

  /**
   * You are given two linked lists representing two non-negative numbers.
   * The digits are stored in reverse order and each of their nodes contain a single digit.
   * Add the two numbers and return it as a linked list.
    Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
    Output: 7 -> 0 -> 8
   * */
  class add_two_numbers {
  public:
    static common::list_node<int> *add(common::list_node<int> *a, common::list_node<int> *b) {
      common::list_node<int> header;
      common::list_node<int> *actual_ptr = &header;

      bool carry = false;
      while (a != nullptr || b != nullptr) {
        int number = 0;
        if (a) {
          number += a->value;
        }
        if (b) {
          number += b->value;
        }
        if (carry) {
          number += 1;
        }
        if (number >= 10) {
          number -= 10;
          carry = true;
        } else {
          carry = false;
        }
        a = a->next;
        b = b->next;
        actual_ptr->next = new common::list_node<int>(number);
        actual_ptr = actual_ptr->next;
      }
      if (carry) {
        actual_ptr->next = new common::list_node<int>(1);
      }
      return header.next;
    }

    static void test() {
      common::list_node<int> a1(2);
      common::list_node<int> a2(4);
      common::list_node<int> a3(3);

      common::list_node<int> b1(5);
      common::list_node<int> b2(6);
      common::list_node<int> b3(4);

      a1.next = &a2;
      a2.next = &a3;
      b1.next = &b2;
      b2.next = &b3;

      auto result = add(&a1, &b1);
      result->print();

      common::list_node<int>::release(result);
    }
  };
}

#endif //PROGRAM_CREEK_ALGORITHMS_1_ADD_TWO_NUMBERS_H
