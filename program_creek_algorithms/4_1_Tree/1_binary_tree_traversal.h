#ifndef CODECHALLENGES_1_BINARY_TREE_TRAVERSAL_H
#define CODECHALLENGES_1_BINARY_TREE_TRAVERSAL_H

#include "../common/tree.h"
#include <stack>
#include <queue>
#include <vector>

namespace four {
	class binary_tree_traversal {
	public:
		template<typename T>
		static std::vector <T> preorder(common::tree_node<T> *root) {
			std::stack < common::tree_node<T> * > stack_tree;
			std::vector <T> ret_val;
			stack_tree.push(root);
			while (!stack_tree.empty()) {
				auto node = stack_tree.top();
				stack_tree.pop();
				ret_val.push_back(node->value);
				if (node->right) {
					stack_tree.push(node->right);
				}
				if (node->left) {
					stack_tree.push(node->left);
				}
			}
			return ret_val;
		}

		template<typename T>
		static std::vector <T> inorder(common::tree_node<T> *root) {
			std::vector <T> ret_val;
			std::stack < common::tree_node<T> * > s1;
			common::tree_node<T> *aux_node = root;
			while (!s1.empty() || aux_node != nullptr) {
				if (aux_node != nullptr) {
					s1.push(aux_node);
					aux_node = aux_node->left;
				} else {
					aux_node = s1.top();
					s1.pop();
					ret_val.push_back(aux_node->value);
					aux_node = aux_node->right;
				}
			}
		}

		template<typename T>
		static std::vector <T> postorder(common::tree_node<T> *root) {
			std::vector <T> ret_val;
			std::stack < common::tree_node<T> * > s1;
			std::stack < common::tree_node<T> * > s2;

			s1.push(root);

			while (!s1.empty()) {
				auto node = s1.top();
				s1.pop();
				s2.push(node->val);
				if (node->left) { s1.push(node->left); }
				if (node->right) { s1.push(node->right); }
			}

			while (!s2.empty()) {
				auto node = s2.top();
				s2.pop();
				ret_val.push_back(node);
			}

			return ret_val;
		}

		template<typename T>
		static std::vector<std::vector <T>> level_order(common::tree_node<T> *root) {
			std::queue<common::tree_node<T>*> actual_nodes;
			std::queue<common::tree_node<T>*> next_nodes;
			std::vector<T> single_line;
			std::vector<std::vector<T>> ret_val;

			actual_nodes.push(root);
			while(!actual_nodes.empty()) {
				auto single_node = actual_nodes;
				single_line.push_back(single_node->value);
				actual_nodes.pop();

				if (single_node->left) {
					next_nodes.push(single_node->left);
				}
				if (single_node->right) {
					next_nodes.push(single_node->right);
				}
				if (actual_nodes.empty) {
					ret_val.push_back(single_line);
					actual_nodes = next_nodes;
					next_nodes.clear();
					single_line.clear();
				}
			}
			return ret_val;
		}
	};
}

#endif //CODECHALLENGES_1_BINARY_TREE_TRAVERSAL_H
