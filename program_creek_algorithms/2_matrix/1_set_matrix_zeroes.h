#ifndef PROGRAM_CREEK_ALGORITHMS_1_SET_MATRIX_ZEROES_H
#define PROGRAM_CREEK_ALGORITHMS_1_SET_MATRIX_ZEROES_H

#include <vector>


namespace two {
  /**
   * Given a m * n matrix, if an element is 0, set its entire row and column to 0.
   * Do it in place.
   * */
  class matrix_zeroes {
  public:
    static void set_zeroes(std::vector<std::vector<int>> &input) {
      bool first_row_has_zero = false;
      bool first_column_has_zero = false;

      if (input.empty()) { return; }
      if (input[0].empty()) { return; }

      for (std::size_t row = 0; row < input.size(); ++row) {
        if (input[row][0] == 0) {
          first_column_has_zero = true;
          break;
        }
      }

      for (std::size_t column = 0; column < input[0].size(); ++column) {
        if (input[0][column] == 0) {
          first_row_has_zero = true;
          break;
        }
      }

      // Now mark on the first row, the columns that must be set to 0
      // and in the on the first column, the row that must be set to 0
      for (std::size_t i = 1; i < input.size(); ++i) {
        for (std::size_t j = 1; j < input[i].size(); ++j) {
          if (input[i][j] == 0) {
            input[0][j] = 0;
            input[i][0] = 0;
          }
        }
      }

      // Now set the matrix
      for (std::size_t i = 1; i < input.size(); ++i) {
        if (input[i][0] == 0) {
          for (std::size_t j = 1; j < input[i].size(); ++j) {
            input[i][j] = 0;
          }
        }
      }
      for (std::size_t i = 1; i < input[0].size(); ++i) {
        if (input[0][i] == 0) {
          for (std::size_j = 1; j < input.size(); ++j) {
            input[j][i] = 0;
          }
        }
      }

      if (first_column_has_zero) {
        for (std::size_t i = 0; i < input.size(); ++i) {
          input[i][0] = 0;
        }
      }
      if (first_row_has_zero) {
        for (std::size_t i = 0; i < input[0].size(); ++i) {
          input[0][i] = 0;
        }
      }
    }
  };
}
#endif //PROGRAM_CREEK_ALGORITHMS_1_SET_MATRIX_ZEROES_H
