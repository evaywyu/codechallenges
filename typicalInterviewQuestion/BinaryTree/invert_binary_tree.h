void invert_binary_tree_helper(TreeNode* root, TreeNode* left, TreeNode* right) {
  if (root == nullptr) return;
  root->right = left;
  root->left = right;
  if(left != nullptr)
    invert_binary_tree_helper(left, left->left, left->right);
  if(right != nullptr)
    invert_binary_tree_helper(right, right->left, right->right);
}

void invert_binary_tree(TreeNode* root) {
  if (root == nullptr) return;
  invert_binary_tree_helper(root, root->left, root->right);
}

void iterative_invert_binary_tree(TreeNode* root) {
	if(root == nullptr) return;

	std::stack<TreeNode*> s;
	s.push(root);
	while(!s.empty()){
		TreeNode* actualNode = s.top();
		s.pop();

		if(actualNode->left != nullptr)
			s.push(actualNode->left);
		if(actualNode->right != nullptr)
			s.push(actualNode->right);

		TreeNode* temp = actualNode->left;
		actualNode->left = actualNode->right;
		actualNode->right = temp;
	}
}