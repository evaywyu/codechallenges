#include <deque>
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:

    vector<vector<int>> levelOrder(TreeNode* root) {
        std::vector<std::vector<int>> result;
        std::vector<int> nodeValues;
        if(root == nullptr){ return result; }
        
        std::deque<TreeNode*> currentNode;
        std::deque<TreeNode*> nextNode;
        
        currentNode.push_back(root);
        
        while(!currentNode.empty()){
            TreeNode* node = currentNode.front();
            currentNode.pop_front();
            if(node->left != nullptr){
                nextNode.push_back(node->left);
            }
            if(node->right != nullptr){
                nextNode.push_back(node->right);
            }
            nodeValues.push_back(node->val);
            if(currentNode.empty()){
                currentNode = nextNode;
                nextNode.clear();
                result.push_back(nodeValues);
                nodeValues.clear();
            }
        }
        return result;
    }
};