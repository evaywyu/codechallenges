/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:

	TreeNode* buildTree(vector<int>& inorder, int inStart, int inEnd,
						vector<int>& postorder, int posStart, int posEnd){
		if(inStart > inEnd || posStart > posEnd){
			return nullptr;
		}

		int rootValue = postorder[posEnd];
		TreeNode* root = new TreeNode(rootValue);

		int k = 0;
		for(int i = 0; i < inorder.size(); ++i){
			if(inorder[i] == rootValue){
				k = i;
				break;
			}
		}

		root->left = buildTree(inorder, inStart, k - 1, postorder, posStart, posStart + k - (inStart+1));
		// K is not the lenght, it need to -(inStart+1) to get the length
		root->right = buildTree(inorder, k + 1, inEnd, postorder, posStart + k - inStart, posEnd - 1);

		return root;
	}

    TreeNode* buildTree(vector<int>& inorder, vector<int>& postorder) {
        int inStart = 0;
        int inEnd = inorder.size() - 1;
        int posStart = 0;
        int posEnd = postorder.size() - 1;

        return buildTree(inorder,inStart,inEnd,postorder,posStart,posEnd);
    }
};