/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<vector<int>> levelOrderBottom(TreeNode* root) {
        std::vector<std::vector<int>> result;
        std::vector<int> singleEntry;
        
        if(root == nullptr){ return result; }
        
        queue<TreeNode*> st;
        queue<TreeNode*> next;
        st.push(root);
        while(!st.empty()){
            
            TreeNode* aux = st.front();
            st.pop();
            singleEntry.push_back(aux->val);
            if(aux->left != nullptr){
                next.push(aux->left);
            }
            if(aux->right != nullptr){
                next.push(aux->right);
            }
            if(st.empty()){
                st = next;
                next = queue<TreeNode*>();
                result.push_back(singleEntry);
                singleEntry.clear();
            }
        }
        // Reverse 
        std::reverse(result.begin(),result.end());
    }
};