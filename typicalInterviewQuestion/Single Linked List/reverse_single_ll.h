class ListNode{
	ListNode(int v) : val(v), next(nullptr){}
	int val;
	ListNode* next;
};

ListNode* reverse_linked_list(ListNode* head) {
  if (head == nullptr) return nullptr;
  ListNode* auxN = new ListNode(head->val);
  while (head->next != nullptr) {
    head = head->next;
    ListNode* tmp = new ListNode(head->val);
    tmp->next = auxN;
    auxN = tmp;
  }
  return auxN;
}


ListNode* reverse_range_linked_list(ListNode* head,int m, int n) {
  if (head == nullptr) return nullptr;
  if (m == n) return head;
  if (n < m) return nullptr;

  ListNode* prev = nullptr;
  ListNode* first = nullptr;
  ListNode* last = nullptr;

  int i = 0;
  ListNode* actual = head;
  while (actual != nullptr) {
    ++i;
    if (i == m - 1) {
      prev = actual;
    }
    if (i == m) {
      first = actual;
    }
    if (i == n) {
      last = actual;
      break;
    }
    actual = actual->next;
  }

  if (first->next == nullptr) {
    return head;
  }

  ListNode* p1 = first->next;
  ListNode* p2 = p1->next;
  p1->next = last->next;

  while (p1 != nullptr && p2 != nullptr) {
    ListNode* tmp = p2->next;
    p2->next = p1;
    p1 = p2;
    p2 = tmp;
  }

  if (prev != nullptr)
    prev->next = p1;
  else return p1;

  return head;
}

void print_list(ListNode* a) {
  while (a != nullptr) {
    std::cout << a->val;
    a = a->next;
  }
  std::cout << "\n";
}