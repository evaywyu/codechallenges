/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
   ListNode* swapPairs(ListNode* head) {
        if (head == nullptr || head->next == nullptr) return head;
        ListNode h(0);
        h.next = head;
        ListNode* p = &h;
        
        while (p->next != nullptr && p->next->next != nullptr) {
          ListNode* t1 = p;
          p = p->next;
          t1->next = p->next;
        
          ListNode* t2 = p->next->next;
          p->next->next = p;
          p->next = t2;
        }
        return h.next;
    }
};