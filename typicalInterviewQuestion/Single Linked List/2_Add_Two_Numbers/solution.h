/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
  ListNode *addTwoNumbers(ListNode *l1, ListNode *l2) {
    if (l1 == nullptr && l2 == nullptr) {
      return nullptr;
    }
    if (l1 == nullptr && l2 != nullptr) {
      return l2;
    }
    if (l1 != nullptr && l2 == nullptr) {
      return l1;
    }
    int carry = 0;

    ListNode *newHead = new ListNode(0);
    ListNode *p1 = l1;
    ListNode *p2 = l2;
    ListNode *p3 = newHead;

    while (p1 != nullptr || p2 != nullptr) {
      if (p1 != nullptr) {
        carry += p1->val;
        p1 = p1->next;
      }

      if (p2 != nullptr) {
        carry += p2->val;
        p2 = p2->next;
      }

      p3->next = new ListNode(carry % 10);
      p3 = p3->next;
      carry /= 10;
    }

    if (carry == 1)
      p3->next = new ListNode(1);

    return newHead->next;
  }
};
