#include <queue>
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
  struct COMP {
    inline bool operator()(const ListNode* a, const ListNode* b) const {
      return a->val > b->val;
    }
  };
  ListNode* mergeKLists(vector<ListNode*>& lists) {
    if (lists.empty()) return nullptr;
    std::priority_queue<ListNode*, std::vector<ListNode*>, COMP> orderedNodes;

    // Add the first node of each list
    for (int i = 0; i < lists.size(); ++i){
        if(lists[i] != nullptr){
            orderedNodes.push(lists[i]);
        }
    }

    // Get the first element of que priorityQueue
    if(orderedNodes.empty()){ return nullptr; }
    ListNode* firstNode = orderedNodes.top();
    ListNode* backNode = firstNode;
    orderedNodes.pop();
    if(firstNode->next != nullptr){
        orderedNodes.push(firstNode->next);
    }

    while (!orderedNodes.empty()) {
      ListNode * tempNode = orderedNodes.top();
      orderedNodes.pop();

      backNode->next = tempNode;
      if (tempNode->next != nullptr) {
        orderedNodes.push(tempNode->next);
      }
      backNode = backNode->next;
    }

    return firstNode;

  }
};
