#include "cascache.h"
#include <iostream>

cuda::Cache& cuda::Cache::getSingleton()
{
	// Static initialization of the cache
	static cuda::Cache m_singleton;
	return m_singleton;
}


cuda::Cache::Cache(const cuda::cache_configuration& config)
  : m_configurationParameters(config),
    m_cacheSize(0),
    m_cacheHits(0)
{
}

cuda::Cache::~Cache()
{
	boost::mutex::scoped_lock _lock(m_resourceMutex);
	m_entries.clear();
	m_lifeTime.clear();
}

void cuda::Cache::clear()
{
	boost::mutex::scoped_lock _lock(m_resourceMutex);
	m_entries.clear();
	m_lifeTime.clear();
	m_cacheSize = 0;
	m_cacheHits = 0;
}


int cuda::Cache::add(const cuda::CacheEntry& entry)
{
	int retVal = 0;
	if(entry.getHash().empty()) return retVal;
	boost::mutex::scoped_lock _lock(m_resourceMutex);
	std::map<std::string,boost::shared_ptr<CacheEntry> >::iterator entriesIt;
	if(entry.getSize() > m_configurationParameters.max_size()) {
		return retVal; // Can't store this value, is too big
	}
	if(m_entries.find(entry.getHash()) == m_entries.end()){
		try{
			// Check entries and size if valid add one, if not remove entries until it fits
			while( (m_entries.size() >= m_configurationParameters.max_entries())  ||
				   (m_cacheSize + entry.getSize() >= m_configurationParameters.max_size())){
				if(m_entries.empty()) { return retVal; } // Entry too big
				// Remove it from the entries and the lifetime set
				std::list<boost::shared_ptr<CacheEntry> >::iterator 
				  setElement = m_lifeTime.begin();
				if( (entriesIt = m_entries.find((*setElement)->getHash())) != m_entries.end()){
					m_cacheSize -= entriesIt->second->getSize();
					m_entries.erase((*setElement)->getHash());
				}
				m_lifeTime.erase((*setElement)->cache_position);
			}
			struct timespec ts;
			if(clock_gettime(CLOCK_MONOTONIC,&ts) == 0){
				boost::shared_ptr<cuda::CacheEntry> newEntry(new cuda::CacheEntry(entry));
				newEntry->mutableLastModification() = static_cast<uint64_t>(ts.tv_sec) * 1000 
			      + static_cast<uint64_t>(ts.tv_nsec) / 1000000;
				m_entries[entry.getHash()] = newEntry;
				newEntry->cache_position = m_lifeTime.insert(m_lifeTime.end(), newEntry);
				m_cacheSize += newEntry->getSize();
				retVal = 1;
			}
		}catch(std::bad_alloc& badAllocation){
			retVal = 0;
		}
	}else{
		// Entry already exists
		std::cout << "Entry already exists" << std::endl;
		retVal = 2;
	}
	return retVal;
}

bool cuda::Cache::get(const std::string& key, cuda::CacheEntry& value)
{
	bool retVal = false;
	if(key.empty()) return retVal;
	boost::mutex::scoped_lock _lock(m_resourceMutex);
	std::map<std::string,boost::shared_ptr<CacheEntry> >::iterator element;
	if((element = m_entries.find(key)) != m_entries.end()){
		struct timespec ts;
		if(clock_gettime(CLOCK_MONOTONIC,&ts) == 0){
			// Update de time, remove it from the set and add it again
			element->second->mutableLastModification() = static_cast<uint64_t>(ts.tv_sec) * 1000 
			  + static_cast<uint64_t>(ts.tv_nsec) / 1000000;
			m_lifeTime.erase(element->second->cache_position);
			element->second->cache_position = m_lifeTime.insert(m_lifeTime.end(), element->second);
			retVal = true;
			m_cacheHits++;
			value = (*element->second);
		}
	}
	return retVal;
}

bool cuda::Cache::update()
{
	bool retVal = false;
	boost::mutex::scoped_lock _lock(m_resourceMutex);
	// Get the actual time
	struct timespec ts;
	if(clock_gettime(CLOCK_MONOTONIC,&ts) != 0){
		return retVal; // Error, couldn't get the clock
	}
	uint64_t actualTime = static_cast<uint64_t>(ts.tv_sec) * 1000 
	  + static_cast<uint64_t>(ts.tv_nsec) / 1000000;
	std::list<boost::shared_ptr<CacheEntry> >::iterator eraseBegin = m_lifeTime.begin();
	std::list<boost::shared_ptr<CacheEntry> >::iterator eraseEnd = m_lifeTime.begin();
	std::list<boost::shared_ptr<CacheEntry> >::iterator itBegin;
	for(itBegin = m_lifeTime.begin();
		itBegin != m_lifeTime.end();
	    ++itBegin,++eraseEnd) {
		// example, yesterday + 1 day < today
		if((*itBegin)->getLastModification() + 
			m_configurationParameters.max_lifetime() 
			< actualTime){
			if((*itBegin)->getHash().empty()) continue;
			// Remove the size from the actual size
			m_cacheSize -= (*itBegin)->getSize();
			// Remove this entry for the elements map
			m_entries.erase((*itBegin)->getHash());
		}else {
			// Don't update the iterator
			break;
		}
	}
	if(eraseBegin != eraseEnd) {
		m_lifeTime.erase(eraseBegin,eraseEnd);
	}
	return retVal;
}

bool cuda::Cache::updateCreate(const cuda::CacheEntry& entry)
{
	bool retVal = false;
	if(entry.getHash().empty()) return retVal;
	boost::mutex::scoped_lock _lock(m_resourceMutex);
	std::map<std::string,boost::shared_ptr<CacheEntry> >::iterator element;
	if((element = m_entries.find(entry.getHash())) != m_entries.end()){
		struct timespec ts;
		uint64_t oldSize = element->second->getSize();
		// Copy the value before smash it with the new value
		std::list<boost::shared_ptr<CacheEntry> >::iterator auxPosition = element->second->cache_position;
		(*(element->second)) = entry; // assign new content, don't care about time
		if(clock_gettime(CLOCK_MONOTONIC,&ts) == 0){
			// Update de time, remove it from the set and add it again
			element->second->mutableLastModification() = static_cast<uint64_t>(ts.tv_sec) * 1000 
			  + static_cast<uint64_t>(ts.tv_nsec) / 1000000;
			m_lifeTime.erase(auxPosition);
			element->second->cache_position = m_lifeTime.insert(m_lifeTime.end(),element->second);
			m_cacheSize -= oldSize;
			m_cacheSize += entry.getSize();
			retVal = true;
		}
	}else {
		// Add it new entry
		try{
			// Check entries and size if valid add one, if not remove entries until it fits
			while( (m_entries.size() >= m_configurationParameters.max_entries())  ||
				   (m_cacheSize + entry.getSize() >= m_configurationParameters.max_size())){
				if(m_entries.empty()) { return retVal; } // Entry too big
				// Remove it from the entries and the lifetime set
				std::list<boost::shared_ptr<CacheEntry> >::iterator setElement =
				  m_lifeTime.begin();
				m_cacheSize -= (*setElement)->getSize();
				m_entries.erase((*setElement)->getHash());
				m_lifeTime.erase(setElement);
			}
			struct timespec ts;
			if(clock_gettime(CLOCK_MONOTONIC,&ts) == 0){
				boost::shared_ptr<cuda::CacheEntry> newEntry(new cuda::CacheEntry(entry));
				newEntry->mutableLastModification() = static_cast<uint64_t>(ts.tv_sec) * 1000 
			      + static_cast<uint64_t>(ts.tv_nsec) / 1000000;
				m_entries[entry.getHash()] = newEntry;
				newEntry->cache_position = m_lifeTime.insert(m_lifeTime.end(),newEntry);
				m_cacheSize += newEntry->getSize();
				retVal = true;
			}
		}catch(std::bad_alloc& badAllocation){
			retVal = false;
		}
	}
	return retVal;
}


void cuda::Cache::setConfiguration(const cuda::cache_configuration& config)
{
	// We block here because it is an important operation and it wouldn't
	// be done so often
	boost::mutex::scoped_lock _lock(m_resourceMutex);
	m_configurationParameters = config;
}
