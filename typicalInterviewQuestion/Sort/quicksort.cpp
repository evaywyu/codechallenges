#include <vector>
#include <iostream>

template<typename T>
void quicksort(std::vector<T>& v, int64_t begin, int64_t end) {
  if (begin < end) {
    auto pivotValue = v[(begin + end) / 2];;
    auto left = begin;
    auto right = end;

    while (left <= right) {
      while (v[left] < pivotValue) {
        left++;
      }
      while (v[right] > pivotValue) {
        right--;
      }
      if (left <= right) {
        std::swap(v[left], v[right]);
        left++;
        right--;
      }
    }
    if (begin < right) {
      quicksort(v, begin, right);
    }
    if (left < end) {
      quicksort(v, left, end);
    }
  }
}


int main(int argc, char** argv) {

  std::vector<int> temp = { 9,8,7,5,1,10,28 };

  quicksort(temp,0,temp.size()-1);
   
  for (auto i : temp) {
    std::cout << i << " ";
  }
  std::cout << '\n';
}