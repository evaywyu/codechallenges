template<typename T>
void merge(std::vector<T>& solution, const std::vector<T>& a, const std::vector<T>& b) {
  int m = a.size();
  int n = b.size();
  solution.resize(n + m);
  int c = solution.size();
  while (m>0 && n>0 && c>0) {
    if (a[m - 1] > b[n - 1]) {
      solution[c - 1] = a[m - 1];
      m--;
    }
    else {
      solution[c - 1] = b[n - 1];
      n--;
    }
    c--;
  }
  while (n>0) {
    solution[c - 1] = b[n - 1];
    n--;
    c--;
  }
  while (m>0) {
    solution[c - 1] = a[m - 1];
    m--;
    c--;
  }
}

template<typename T>
void mergesort(std::vector<T>& v) {
  if (1 < v.size()) {
    std::vector<T> arr1(v.begin(), v.begin() + v.size() / 2);
    std::vector<T> arr2(v.begin() + v.size() / 2, v.end());
    mergesort(arr1);
    mergesort(arr2);
    merge(v, arr1, arr2);
  }
}