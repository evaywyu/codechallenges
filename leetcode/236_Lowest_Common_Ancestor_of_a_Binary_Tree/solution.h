/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    struct LCAHelper{
    public:
        int count = 0;
        TreeNode* node;

        LCAHelper(int c, TreeNode* n) : count(c), node(n) {}
    };

    LCAHelper helperMethod(TreeNode* root, TreeNode* p, TreeNode* q) {
        if(root == nullptr){
            return LCAHelper(0,nullptr);
        }

        LCAHelper left = helperMethod(root->left,p,q);
        if(left.count == 2){
            return left;
        }
        LCAHelper right = helperMethod(root->right,p,q);
        if(right.count == 2){
            return right;
        }

        int total = left.count + right.count;
        if(root == p || root == q){
            total++;
        }
        return LCAHelper(total,root);
    }

    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
        return helperMethod(root,p,q).node;
    }
};
