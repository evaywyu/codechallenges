## First Solution
class Solution:
    def isPalindrome(self, x: int) -> bool:
        """ could be made faster by check negated value """
        return str(x) == str(x)[::-1]

## No Str Cast Solution
class Solution:
    def isPalindrome(self, x: int) -> bool:
        """ Reversing the int with maths """
        original = x
        reversed = 0
        while original > 0:
            reversed = (reversed * 10) + (original % 10)
            original //= 10
        return reversed == x


# test case 121
# test case 10
# test case