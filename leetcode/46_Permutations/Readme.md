Given a collection of numbers, return all possible permutations.

For example,
[1,2,3] have the following permutations:
[1,2,3], [1,3,2], [2,1,3], [2,3,1], [3,1,2], and [3,2,1].

Loop through the array, in each iteration, a new number is added to different locations of
results of previous iteration. Start from an empty List.