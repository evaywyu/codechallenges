class Solution {
public:
	void permute(vector<int> nums, int start, vector<vector<int>> & result){
		if(start >= nums.size()){
			result.push_back(nums);
		}

		for(int j = start; j < nums.size(); ++j){
			std::swap(nums[start],nums[j]);
			permute(nums,start+1,result);
			std::swap(nums[start],nums[j]);
		}
	}
    vector<vector<int>> permute(vector<int>& nums) {
        vector<vector<int>> result;
        permute(nums,0,result);
        return result;
    }
};