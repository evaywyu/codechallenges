class Solution {
public:
    bool isValid(string s) {
        std::stack<char> actualList;
        for(int i= 0; i < s.size(); ++i){
            if(s[i] == '{' || s[i] == '(' || s[i] == '['){
                actualList.push(s[i]);
            }else {
                if(!actualList.empty()){
                    char topElemen = actualList.top();
                    if( (topElemen == '(' && s[i] != ')') ||
                        (topElemen == '{' && s[i] != '}') ||
                        (topElemen == '[' && s[i] != ']') ){
                            return false;
                        }
                    actualList.pop();
                }else{
                    return false;
                }
            }
        }
        if(actualList.empty()) return true;
        return false;
    }
};