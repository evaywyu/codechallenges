class Solution:
    """ Using a list as stack, LIFO """
    def isValid(self, s: str) -> bool:
        stack = [] # holds next value to close parantheses expected
        charmap = {'(': ')', '{': '}', '[': ']'}
        for c in s:
            if c in charmap.keys():
                stack.append(charmap.get(c))
            elif c in charmap.values():
                if not stack:
                    return False
                if c != stack.pop():
                    return False
        return not stack
