Given a string containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.

The brackets must close in the correct order, "()" and "()[]{}" are all valid but "(]" and "([)]" are not.

--

Pretty simple program doable with a simple stack, everytime se an opener we push it on the stack, when we see a close we pop the stack and check if it matches or not