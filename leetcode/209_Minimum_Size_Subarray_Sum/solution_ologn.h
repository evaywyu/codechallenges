class Solution {
public:
    void search(vector<int> &ss, int st, int p, int q, int &res, int &s){
        if (ss[q]-ss[st] >= s && p < q){
            res = min(res, q-st);    
            int mid = p + (q-p)/2;
            if (ss[mid]-ss[st] >= s){
                search(ss, st, p, mid, res, s);
            }else{
                if (mid != p){ 
                    search(ss,st, mid ,q,res, s);
                }
          }
        }
    }
     
    int minSubArrayLen(int s, vector<int>& nums) {
        int n = nums.size();
        if (n==0) return 0;
        vector<int> ss(n+1,0);
        int res = n;
         
        for (int i=0;i<n;i++){
            ss[i+1] = ss[i] + nums[i];
        }
         
        if (ss[n] < s) return 0;
         
        for (int i=0;i<n;i++){
            search(ss,i, i, n,res,s);
        }
         
        return res;
    }
};