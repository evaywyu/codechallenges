class Solution:
    """ Top down method. There is also a backwards method that works too"""
    def romanToInt(self, s: str) -> int:
        romanNumMap = {'I': 1,'V': 5,'X': 10,'L': 50,'C': 100,'D': 500,'M': 1000}
        sum = 0
        prev = [0]
        for c in s:
            sum += romanNumMap[c]
            if prev == 'I' and c in ['V', 'X']:
                sum -= 2
            if prev == 'X' and c in ['L', 'C']:
                sum -= 20
            if prev == 'C' and c in ['D', 'M']:
                sum -= 200
            prev = c
        return sum