class Solution {
public:

    int romanToInt(string s) {
        int retVal = 0;
        for(int i = 0; i < s.size();++i){
            if(s[i] == 'M'){
                retVal += 1000; 
            }else if(s[i] == 'C'){
                if( i+1 < s.size() && (s[i+1] == 'M' || s[i+1] == 'D')){
                    retVal -= 100;   
                } else{
                    retVal += 100;
                }
            }else if(s[i] == 'D'){
                retVal += 500;
            }else if(s[i] == 'X'){
                if( i +1 < s.size() && (s[i+1] == 'C' || s[i+1] == 'L')){
                    retVal -= 10;
                }else{
                    retVal += 10;
                }
            }else if(s[i] == 'L'){
                retVal += 50;
            }else if(s[i] == 'V'){
                retVal += 5;
            }else if(s[i] == 'I'){
                if(i+1 < s.size() && (s[i+1] == 'X' || s[i+1] == 'V')){
                    --retVal;
                }else{
                    ++retVal;
                }
            }
        }
        return retVal;
    }
};