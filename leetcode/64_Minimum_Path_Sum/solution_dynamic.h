class Solution{
public:

  int minPathSum(vector<vector<int>>& grid){
    if(grid.empty() || grid[0].empty()) return 0;
    
    int m = grid.size();
    int n = grid[0].size();

    vector<vector<int>> result(m);
    for(int i = 0; i < m; ++i){
      result[i].resize(n);
    }
   
    result[0][0] = grid[0][0];
    // Top row
    for(int i = 1; i < n; ++i){
      result[0][i] = result[0][i-1] + grid[0][i];
    }
    // Left column
    for(int j = 1; j < m; ++j){
      result[j][0] = result[j-1][0] + grid[j][0];
    }

    // Fill the rest
    for(int i = 1; i < m; ++i){
      for(int j = 1; j < n; ++j){
        if(result[i-1][j] > result[i][j-1]){
          result[i][j] = result[i][j-1] + grid[i][j];
        }else{
          result[i][j] = result[i-1][j] + grid[i][j];
        }
      }
    }
    return result[m-1][n-1];
  }
};
