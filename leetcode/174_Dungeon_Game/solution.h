class Solution {
public:
    int calculateMinimumHP(vector<vector<int>>& dungeon) {
        int m = dungeon.size();
        int n = dungeon[0].size();

        // Init dp table
        std::vector<std::vector<int>> h(m,std::vector<int>(n));
        h[m-1][n-1] = std::max(1 - dungeon[m-1][n-1],1);

        // Init last row
        for(int i = m - 2; i >= 0; --i){
        	h[i][n-1] = std::max(h[i+1][n-1] - dungeon[i][n-1],1);
        }

        // Init last column
        for(int j = n -2 ; j >= 0; --j){
        	h[m-1][j] = std::max(h[m-1][j+1] - dungeon[m-1][j],1);
        }

        // Calcualte dp table
        for(int i = m - 2; i >= 0; --i){
        	for(int j = n - 2; j >= 0; --j){
        		int down = std::max(h[i+1][j] - dungeon[i][j],1);
        		int right = std::max(h[i][j+1] - dungeon[i][j],1);
        		h[i][j] = std::min(right,down);
        	}
        }
        return h[0][0];
    }
};