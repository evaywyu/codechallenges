class Solution {
public:
    vector<int> spiralOrder(vector<vector<int>>& matrix) {
        std::vector<int> result;

        if(matrix.empty()) return result;
        int m = matrix.size();
        int n = matrix[0].size();
        int x = 0;
        int y = 0;
      
        while(m>0 && n>0){
          if(m == 1){
            for(int i = 0; i < n; ++i){
              result.push_back(matrix[x][y++]);
            }
            break;
          }else if(n == 1){
            for(int i = 0; i < m; ++i){
              result.push_back(matrix[x++][y]);
            }
            break;
          }
    
          // Circle
          // top - move right
          for(int i = 0; i < n - 1; ++i){
            result.push_back(matrix[x][y++]);
          }
          // right - move down
          for(int i = 0; i < m - 1; ++i){
            result.push_back(matrix[x++][y]);
          }
          // Bottom - move left
          for(int i = 0; i < n - 1; ++i){
            result.push_back(matrix[x][y--]);
          }
          // Left - move up
          for(int i = 0; i < m - 1; ++i){
            result.push_back(matrix[x--][y]);
          }
    
          x++;
          y++;
          m = m - 2;
          n = n - 2;
        }
        return result;
    }
};