Given an array of strings, group anagrams together.

For example, give: "eat", "tea", "tan", "ate", "nat", "bat"

Return

["ate","eat","tea"]
["nat","tan"]
["bat"]

for the return value, each inner list elements must follow lexicographic order

all inputs will be in lower case


---


The basic idea is to sort each word before add them to a map, storing the indexes of those entries
Now with those index construct the response.
