class Solution {
public:
    vector<vector<string>> groupAnagrams(vector<string>& strs) {
        std::vector<std::vector<string>> result;
        if(strs.empty()){
          return result;
        }
    
        std::map<std::string,std::vector<int>> hash;
        for(size_t i = 0; i < strs.size(); ++i){
          std::string aux = strs[i];
          std::sort(aux.begin(),aux.end());
          auto element = hash.find(aux);
          if(element == hash.end()){
            hash.insert(std::make_pair(aux,std::vector<int>({static_cast<int>(i)})));
          }else{
            element->second.push_back(i);
          }
        }
    
        for(auto it = hash.begin(); it != hash.end(); ++it){
            std::vector<std::string> single(it->second.size());
            for(size_t i = 0; i < it->second.size(); ++i){
              single[i] = strs[it->second[i]];
            }
            std::sort(single.begin(),single.end());
            result.push_back(single);
        }
        return result;
    }
};