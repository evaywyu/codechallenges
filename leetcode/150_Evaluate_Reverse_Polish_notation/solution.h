class Solution {
public:
    int evalRPN(vector<string>& tokens) {
       int retVal = 0;
       std::stack<string> values;
       for(int i = 0; i < tokens.size(); ++i){
           if(tokens[i] == "+"){
               // Pop two elements add them
               // and push the result
               int val1,val2;
               val1 = atoi(values.top().c_str());
               values.pop();
               val2 = atoi(values.top().c_str());
               values.pop();
               std::stringstream sb;
               sb << val2 + val1;
               values.push(sb.str());
           }else if(tokens[i] == "-"){
               int val1,val2;
               val1 = atoi(values.top().c_str());
               values.pop();
               val2 = atoi(values.top().c_str());
               values.pop();
               std::stringstream sb;
               sb << val2 - val1;
               values.push(sb.str());
           }else if(tokens[i] == "*"){
               int val1,val2;
               val1 = atoi(values.top().c_str());
               values.pop();
               val2 = atoi(values.top().c_str());
               values.pop();
               std::stringstream sb;
               sb << val2 * val1;
               values.push(sb.str());
           }else if(tokens[i] == "/"){
               int val1,val2;
               val1 = atoi(values.top().c_str());
               values.pop();
               val2 = atoi(values.top().c_str());
               values.pop();
               std::stringstream sb;
               sb << val2 / val1;
               values.push(sb.str());
           }else{
               values.push(tokens[i]);
           }
       }
        retVal = atoi(values.top().c_str());
        return retVal;
    }
};