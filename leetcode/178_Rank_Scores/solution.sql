-- A Lesson Learned in Reading the Instruction Tips
-- In MySQL, put ` around Reserved words when we want
-- to use them as Column Names. 
-- Note: Tested in MS SQL Server without Quotes and
--       It's fine. -.-"
SELECT
    Score,
    DENSE_RANK() OVER (ORDER BY score DESC) AS `Rank`
FROM scores
ORDER BY `Rank`;

