from math import ceil
class Solution:
    def firstBadVersion(self, n):
        """ BST Implementation.
        Uses ceiling function from std math lib
        But can go without as long as it is using int cast

        :type n: int
        :rtype: int
        """
        lower = 0
        upper = n
        while (upper - lower) > 1:
            version = lower + ceil((upper - lower) / 2)
            if isBadVersion(version):
                upper = version
            else:
                lower = version
        return upper


