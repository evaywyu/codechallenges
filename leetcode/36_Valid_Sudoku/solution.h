class Solution {
public:
    bool isValidSudoku(vector<vector<char>>& board) {
    if (board.empty() || board.size() != 9 || board[0].size() != 9) return false;
    // Check the rows
    int line[9];
    for (int i = 0; i < 9; ++i) {
      memset(line, 0, sizeof(int) * 9);
      for (int j = 0; j < 9; ++j) {
          if(board[i][j] != '.'){
              if(line[board[i][j]-'1']){
                  return false;
              }
              line[board[i][j]-'1']++;
          }
      }
    }
    // Check the columns
    for (int i = 0; i < 9; ++i) {
      memset(line, 0, sizeof(int) * 9);
      for (int j = 0; j < 9; ++j) {
        if (board[j][i] != '.') {
            if(line[board[j][i] - '1']){
                return false;
            }
            line[board[j][i] - '1']++;
        }
      }
    }
    // Check each submatrix
    for (int i = 0; i < 9; ++i) {
      memset(line, 0, sizeof(int) * 9);
      for (int k = i / 3 * 3; k < i / 3 * 3 + 3; ++k) {
        for (int h = i % 3 * 3; h < i % 3 * 3 + 3; ++h) {
          if (board[k][h] != '.') {
              if(line[board[k][h] - '1']){
                  return false;
              }
              line[board[k][h] - '1']++;
          }
        }
      }
    }
    return true;
  }
};