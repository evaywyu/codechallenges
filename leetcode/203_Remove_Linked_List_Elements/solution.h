/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* removeElements(ListNode* head, int val) {
        if(head == nullptr) return nullptr;
        ListNode n(0);
        n.next = head;
        ListNode *aux = &n;
        while(aux->next != nullptr){
            if(aux->next->val == val){
                ListNode* tmp = aux->next;
                aux->next = aux->next->next;
                delete tmp; // avoid memleaks
            }else{
                aux = aux->next;
            }
        }
        return n.next;
    }
};