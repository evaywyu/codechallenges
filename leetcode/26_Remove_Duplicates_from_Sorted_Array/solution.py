class Solution:
    """ Simple Iterative approach. 
    Time Complexity: O(n)
    Space Complexity: O(1)
    """
    def removeDuplicates(self, nums: List[int]) -> int:
        head = 0
        tail = 1
        while tail < len(nums):
            if nums[tail] == nums[head]:
                tail += 1
            else:
                head += 1
                nums[head] = nums[tail]
        return head + 1
