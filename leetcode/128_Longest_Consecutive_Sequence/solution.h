class Solution {
public:
    int longestConsecutive(vector<int>& num) {
        int max = 0;
        std::unordered_map<int, int> map;
        for (int i: num) {
            if (map.find(i) != map.end()) continue;
            int left = i, right = i;
            map[i] = i; // need to place it the map for later check
            if (map.find(i-1) != map.end()) left = map[i-1];
            if (map.find(i+1) != map.end()) right = map[i+1];
            map[left] = right;
            map[right] = left;
            max = std::max(max, right - left + 1);
        }
        return max;

        
    }
};