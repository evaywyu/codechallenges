/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
	vector<TreeNode*> generateTrees(int start, int end){
		std::vector<TreeNode*> retVal;

		if(start > end){
			retVal.push_back(nullptr);
			return retVal;
		}

		for(int i = start; i <= end; ++i){
			std::vector<TreeNode*> left = generateTrees(start, i-1);
			std::vector<TreeNode*> right = generateTrees(i+1,end);
			for(int j = 0; j < left.size(); ++j){
				for(int k = 0; k < right.size(); ++k){
					TreeNode* node = new TreeNode(i);
					node->left = left[j];
					node->right = right[k];
					retVal.push_back(node);
				}
			}
		}
		return retVal;
	}
    vector<TreeNode*> generateTrees(int n) {
        return generateTrees(1,n);
    }
};