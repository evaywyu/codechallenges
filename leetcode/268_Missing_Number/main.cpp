#include "solution.h"
#include <iostream>

int main(){
// [9,6,4,2,3,5,7,0,1]
	std::vector<int> nums = {9,6,4,2,3,5,7,0,1};
	Solution a;
	int sol = a.missingNumber(nums);
	std::cout << sol << std::endl;
}
