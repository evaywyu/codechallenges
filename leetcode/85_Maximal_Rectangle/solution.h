class Solution {
public:
	int maxAreaInHistogram(std::vector<int>& height){
		std::stack<int> st;
		int i = 0;
		int max = 0;

		while(i < height.size()){
			if(st.empty() || height[st.top()] <= height[i]){
				st.push(i++);
			}else{
				int t = st.top();
				st.pop();
				max = std::max(max, height[t] * (st.empty()? i : i - st.top() - 1));
			}
		}
		return max;
	}
    int maximalRectangle(vector<vector<char>>& matrix) {
        int m = matrix.size();
        int n = m == 0? 0 : matrix[0].size();
        std::vector<std::vector<int>> heights(m,std::vector<int>(n+1));

        int maxArea = 0;
        for(int i = 0; i < m; ++i){
        	for(int j = 0; j < n; ++j){
        		if(matrix[i][j] == '0'){
        			heights[i][j] = 0;
        		}else{
        			heights[i][j] = i == 0? 1 : heights[i-1][j] + 1;
        		}
        	}
        }
        for(int i = 0; i < m; ++i){
        	int area = maxAreaInHistogram(heights[i]);
        	if(area > maxArea){
        		maxArea = area;
        	}
        }
        return maxArea;
    }
};