class Solution{
public:
  std::vector<std::vector<int>> generateMatrix(int n){
    int total = n * n;
    std::vector<std::vector<int>> result(n);
    for(int i = 0; i < n; ++i){
      result[i].resize(n);
    }
    
    int x = 0;
    int y = 0;
    int step = 0;
    int i = 0;
    
    while(i < total){
      while(y + step < n){
        ++i;
        result[x][y] = i;
        ++y;
      }
      y--;
      x++;

      while(x + step < n){
        ++i;
        result[x][y] = i;
        x++;
      }
      x--;
      y--;

      while(y >= step){
        ++i;
        result[x][y] = i;
        y--;
      }
      y++;
      x--;
      step++;

      while(x >= step){
        i++;
        result[x][y] = i;
        x--;
      }
      x++;
      y++;
    }
    return result;
  }
};
