Given an integer n, generate a square matrix filled with elements from 1 to n2 in
spiral order. For example, given n = 4,
[
[1, 2, 3, 4],
[12, 13, 14, 5],
[11, 16, 15, 6],
[10, 9, 8, 7]
]
