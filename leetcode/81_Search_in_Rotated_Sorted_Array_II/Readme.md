Follow up for "Search in Rotated Sorted Array":
What if duplicates are allowed?

Would this affect the run-time complexity? How and why?

Write a function to determine if a given target is in the array.


-----

Main idea is Binary Search.
As the sequence is rotated, for any mid element, either it is of order with its previous part, or it is of order with its next part. e.g. 561234, middle element 1 has an order with its next part 1234.
                                 5678123, middle element 8 has an order with its previous part 5678.

Normal binary search just compare the middle element with the target, here we need more than that.
e.g.
567123, the middle element is 1, if we want to search 6, first compare middle element with 1st element, to know which part is of order, if mid>1st, the first part is ordered,otherwise the last part is ordered. Then compare the target value with the bound value in each case. Details see the code.

Just when A[mid]==A[st], we cannot say the previous part is ordered, then we just go to the next element and check again.
