/**
 * Definition for binary tree with next pointer.
 * struct TreeLinkNode {
 *  int val;
 *  TreeLinkNode *left, *right, *next;
 *  TreeLinkNode(int x) : val(x), left(NULL), right(NULL), next(NULL) {}
 * };
 */
class Solution {
public:
    void connect(TreeLinkNode *root) {
        if(root == nullptr) 
        	return;

	    TreeLinkNode* lastHead = root;//prevous level's head 
	    TreeLinkNode* lastCurrent = nullptr;//previous level's pointer
	    TreeLinkNode* currentHead = nullptr;//currnet level's head 
	    TreeLinkNode* current = nullptr;//current level's pointer
	 
	    while(lastHead!=nullptr){
	        lastCurrent = lastHead;
	 
	        while(lastCurrent!=nullptr){
	            //left child is not null
	            if(lastCurrent->left!=nullptr)    {
	                if(currentHead == nullptr){
	                    currentHead = lastCurrent->left;
	                    current = lastCurrent->left;
	                }else{
	                    current->next = lastCurrent->left;
	                    current = current->next;
	                }
	            }
	 
	            //right child is not null
	            if(lastCurrent->right!=nullptr){
	                if(currentHead == nullptr){
	                    currentHead = lastCurrent->right;
	                    current = lastCurrent->right;
	                }else{
	                    current->next = lastCurrent->right;
	                    current = current->next;
	                }
	            }
	            lastCurrent = lastCurrent->next;
	        }
	 
	        //update last head
	        lastHead = currentHead;
	        currentHead = nullptr;
	    }
    }
};