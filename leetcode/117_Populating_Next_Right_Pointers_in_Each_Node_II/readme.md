Follow up for problem "Populating Next Right Pointers in Each Node".

What if the given tree could be any binary tree? Would your previous solution still work?

Note:

You may only use constant extra space.
For example,
Given the following binary tree,
         1
       /  \
      2    3
     / \    \
    4   5    7
After calling your function, the tree should look like:
         1 -> NULL
       /  \
      2 -> 3 -> NULL
     / \    \
    4-> 5 -> 7 -> NULL

-----------------------------

Similar to Populating Next Right Pointers in Each Node, we have 4 pointers at 2 levels of the tree.

                      1 -> NULL
LastCurrrent  ---|  /  \
LastHead---------> 2 -> 3 ->NULL
                  / \    \
CurrentHead----> 4   5    7
                 ^
Current----------|