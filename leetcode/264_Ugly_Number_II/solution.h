#include <vector>

class Solution {
public:
  inline int min(int a, int b, int c) {
    if (a <= b) {
      if (a <= c) {
        return a;
      } else {
        return c;
      }
    }
    if (b <= c) {
      return b;
    } else {
      return c;
    }
  }
  int nthUglyNumber(int n) {
    std::vector<int> ugly(n);
    int i2 = 0, i3 = 0, i5 = 0;
    int i;
    int nextMultipleOf2 = 2;
    int nextMultipleOf3 = 3;
    int nextMultipleOf5 = 5;
    int nextUglyN = 1;
    ugly[0] = 1;
    for (i = 1; i < n; ++i) {
      nextUglyN = min(nextMultipleOf2, nextMultipleOf3, nextMultipleOf5);
      ugly[i] = nextUglyN;
      if (nextUglyN == nextMultipleOf2) {
        i2 += 1;
        nextMultipleOf2 = ugly[i2] * 2;
      }
      if (nextUglyN == nextMultipleOf3) {
        i3 += 1;
        nextMultipleOf3 = ugly[i3] * 3;
      }
      if (nextUglyN == nextMultipleOf5) {
        i5 += 1;
        nextMultipleOf5 = ugly[i5] * 5;
      }
    }
    return nextUglyN;
  }
};
