class Solution:
    def mySqrt(self, x: int) -> int:
        """Binary Search."""
        head = 1
        tail = x
        while tail > head:
            factor = (head + tail) // 2
            if (factor * factor) == x:
                return factor 
            if (factor * factor) > x:
                tail = factor
            if (factor * factor) < x:
                head = factor
            if (tail - head) <= 1:
                return head
        return tail
