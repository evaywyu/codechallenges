/**
 * Definition for an interval.
 * struct Interval {
 *     int start;
 *     int end;
 *     Interval() : start(0), end(0) {}
 *     Interval(int s, int e) : start(s), end(e) {}
 * };
 */
class Solution {
public:
    static bool compare(Interval& i, Interval& j){
        return i.start < j.start;
    }
    vector<Interval> insert(vector<Interval>& intervals, Interval newInterval) {
        if(intervals.empty()) {
            intervals.push_back(newInterval);
            return intervals;
        }
        
        std::vector<Interval> results;
        intervals.push_back(newInterval);
        std::sort(intervals.begin(),intervals.end(),compare);
        Interval first = intervals[0];
        for(int i = 1; i < intervals.size(); ++i){
            Interval actual = intervals[i];
            if(first.end >= actual.start){
                first = Interval(first.start,std::max(first.end,actual.end));
            }else{
                results.push_back(first);
                first = actual;
            }
        }
        results.push_back(first);
        return results;
    }
};