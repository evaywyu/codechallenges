class Solution {
public:

    void combinationSum(std::vector<int>& candidates, int target, int j, std::vector<int> curr, std::vector<vector<int>>& result){
        if(target == 0){
            result.push_back(curr);
        }else{
            for(int i = j; i < candidates.size(); ++i){
                if(target < candidates[i]){
                    return;
                }
                
                curr.push_back(candidates[i]);
                combinationSum(candidates,target - candidates[i], i, curr, result);
                curr.pop_back();
            }
        }
    }
    vector<vector<int>> combinationSum(vector<int>& candidates, int target) {
        std::vector<std::vector<int>> result;
        if(candidates.empty()){
            return result;
        }
        
        vector<int> current;
        std::sort(candidates.begin(),candidates.end());
        
        combinationSum(candidates, target, 0, current, result);
        
        return result;
    }
};