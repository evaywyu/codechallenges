class Solution:
    """ First Solution applies O(n**2) Efficiency. """
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        for i in range(len(nums)):
            if nums[i] >= target:
                next
            for j in range(i+1, len(nums)):
                if nums[j] >= target:
                    next
                if nums[i] + nums[j] == target:
                    return[i,j]
