#include <algorithm>
#include <unordered_map>

using namespace std;

class Solution {
public:
  vector<int> twoSum(vector<int> &nums, int target) {
    std::unordered_map<int, int> mapp;
    std::vector<int> result;

    for (size_t i = 0; i < nums.size(); ++i) {
      auto itMap = mapp.find(nums[i]);
      if (itMap != mapp.end()) {
        int index = mapp[nums[i]];
        return std::vector<int>({index + 1, i + 1});
      } else {
        mapp[target - nums[i]] = i;
      }
    }
    return result;
  }
};
