class Solution {
public:
    int strStr(string haystack, string needle) {
        //return haystack.find(needle);
        if(needle.empty()) return 0;
        int haylength = haystack.length();
        int needlength = needle.length();
        int retVal = -1;
        for(int i = 0; i < haylength && (i + needlength <= haylength); ++i) {
            bool advance = true;
            for(int j = 0; j < needlength && advance && i + j < haylength; ++j){
                if(i + j > haylength){
                    advance = false;
                }
                if(haystack[i+j] != needle[j]){
                    advance = false;
                }
            }
            if(advance == true){
                return i;
            }
        }
        return retVal;
    }
};