class Solution {
public:
    int maxArea(vector<int>& height) {
        if(height.size()<2) return 0;
        int left = 0;
        int right = height.size() - 1;
        int maxValue = 0;
        while(left < right){
            maxValue = std::max(maxValue, (right-left) * std::min(height[left],height[right]));
            if(height[left] < height[right])
                left++;
            else
                right--;
        }
        return maxValue;
    }
};