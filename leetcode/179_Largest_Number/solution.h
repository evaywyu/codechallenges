class Solution {
public:
    static bool comp(int a, int b){
        std::stringstream ss;
        ss << a;
        std::string sa(ss.str());
        ss.clear();
        ss << b;
        std::string sb(ss.str());
        
        sa = sa + sb;
        sb = sb + sa;
        
        if(sa.compare(sb) >= 0){
          return true;
        }else{
          return false;
        }
    }

    string largestNumber(vector<int>& nums) {
        std::string result;
        if(nums.empty()) return std::string();
        if(nums.size() == 1){
            std::ostringstream ss;
            ss << nums[0];
            result = ss.str();
            return result;
        }
        std::sort(nums.begin(),nums.end(),comp);
        if(nums[0] == 0){ return "0"; }
        std::ostringstream ss;
        for(auto i: nums){
            ss << i;
        }
        return ss.str();
    }
};