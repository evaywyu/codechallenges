class Solution:
    def removeElement(self, nums: List[int], val: int) -> int:
        if not nums:
            return 
        begin = end = 0
        while end < len(nums):
            if nums[end] != val:
                nums[begin] = nums[end]
                begin += 1
            end += 1
        return begin
