class Solution {
public:
    int removeElement(vector<int>& nums, int val) {
        std::sort(nums.begin(),nums.end());
        for(auto it = nums.begin(); it != nums.end(); ){
            if(*it == val){
                it = nums.erase(it);
            }else{
                if(*it > val) return nums.size();
                
                it++;
            }
        }
        return nums.size();
    }
};