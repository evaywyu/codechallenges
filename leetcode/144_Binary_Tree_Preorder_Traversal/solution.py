# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def dfs(self, node: TreeNode, values: List[TreeNode]) -> List[int]:
        if node:
            values.append(node.val)
            self.dfs(node.left, values)
            self.dfs(node.right, values)
        
    def preorderTraversal(self, root: TreeNode) -> List[int]:
        values = []
        self.dfs(root, values)
        return values
