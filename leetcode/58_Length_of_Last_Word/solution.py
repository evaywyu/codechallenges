class Solution:
    def lengthOfLastWord(self, s: str) -> int:
        """ This is faster than reversting the string with [::-1] and then 
        split with 1 time and counting first string in returned values. 
        Not sure why.
        """
        strs = s.split(None)
        if strs:
            return len(strs[-1])
        return 0
