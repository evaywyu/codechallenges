class Solution:
    def longestCommonPrefix(self, strs: List[str]) -> str:
        """Vertical Scanning Solution
        Efficiency: min length of string in strs * len(strs) = S
                   O(S)
        """
        max_idx = min(len(s) for s in strs)
        for i in range(max_idx):
            common_char = strs[0][i]
            for s in strs:
                if s[i] != common_char:
                    return s[:i]
        return strs[0][:max_idx]
