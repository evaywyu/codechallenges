class Solution {
public:
    bool isHappy(int n) {
        if(n<=0) return false;
        int actualN = n;
        int currentVal = 0;
        int actualSum = 0;
        int dec = 0;
        std::unordered_set<int> prevResults;
        while(1){
            currentVal = actualN;
            actualSum = 0;
            while(currentVal!=0){
                dec = currentVal % 10;
                currentVal /= 10;
                actualSum += dec * dec;
            }
            if(actualSum == 1){ return true;}
            if(prevResults.find(actualSum) != prevResults.end()){ return false;}
            else{ prevResults.insert(actualSum); }
            actualN = actualSum;
        }
    }
};