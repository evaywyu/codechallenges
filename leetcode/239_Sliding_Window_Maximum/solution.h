#include <deque>

class Solution {
public:
  vector<int> maxSlidingWindow(vector<int> &nums, int k) {
    std::vector<int> result;
    if (nums.empty() || k == 0)
      return result;
    std::deque<int> value;
    for (int i = 0; i < k; ++i) {
      while (!value.empty() && nums[i] >= nums[value.back()]) {
        value.pop_back();
      }
      value.push_back(i);
    }
    for (int i = k; i < nums.size(); ++i) {
      result.push_back(nums[value.front()]);
      while (!value.empty() && nums[i] >= nums[value.back()]) {
        value.pop_back();
      }
      while (!value.empty() && value.front() <= i - k) {
        value.pop_front();
      }
      value.push_back(i);
    }
    result.push_back(nums[value.front()]);
    return result;
  }
};
