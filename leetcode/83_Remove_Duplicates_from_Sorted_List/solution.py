import math 

class Solution:
    def deleteDuplicates(self, head: ListNode) -> ListNode:
        """ O(n) Solution """
        head_ptr = current = ListNode(-math.inf)
        while head:
            if current.val < head.val:
                current.next = head
                current = current.next
            head = head.next 
        current.next = None
        return head_ptr.next
