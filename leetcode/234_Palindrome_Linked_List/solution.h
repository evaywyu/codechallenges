/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    bool isPalindrome(ListNode* head) {
        if(head == nullptr || head->next == nullptr){
            return true;
        }
        // This will increase at double speed
        // To set the middle of the linked list
        ListNode* fast = head;
        ListNode* slow = head;

        while(fast->next != nullptr && fast->next->next != nullptr) {
            slow = slow->next;
            fast = fast->next->next;
        }

        // Slow now points at the middle of the list
        // To keeo O(1) what we are going to do
        // is to reverse the second half
        ListNode* secondHead = slow->next;
        slow->next = nullptr;

        ListNode* pointer1 = secondHead;
        ListNode* pointer2 = pointer1->next;

        while(pointer1 != nullptr && pointer2 != nullptr){
            ListNode* tmp = pointer2->next;
            pointer2->next = pointer1;
            pointer1 = pointer2;
            pointer2 = tmp;
        }

        secondHead->next = nullptr;
        // Compare the two sublists now
        ListNode* p = (pointer2 == nullptr?pointer1:pointer2);
        ListNode* q = head;
        while(p != nullptr){
            if(p->val != q->val){
                return false;
            }
            p = p->next;
            q = q->next;
        }
        return true;
    }
};
