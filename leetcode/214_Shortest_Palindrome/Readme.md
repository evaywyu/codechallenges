Given a string S, you are allowed to convert it to a palindrome by adding characters
in front of it. Find and return the shortest palindrome you can find by performing this
transformation.
For example, given "aacecaaa", return "aaacecaaa"; given "abcd", return "dcbabcd".

-----

Es lineal, coincide la reverse con la otra en algun punto, en ese momento parar.
