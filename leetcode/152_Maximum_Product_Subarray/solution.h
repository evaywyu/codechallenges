class Solution {
public:
    int maxProduct(vector<int>& nums) {
        if(nums.empty()) return 0;
        int maxLocal = nums[0];
        int minLocal = nums[0];
        int global = nums[0];
        
        for(int i = 1; i < nums.size(); ++i){
            int temp = maxLocal;
            maxLocal = std::max(std::max(nums[i]*maxLocal,nums[i]),nums[i]*minLocal);
            minLocal = std::min(std::min(nums[i]*temp, nums[i]), nums[i]*minLocal);
            global = std::max(global,maxLocal);
        }
        return global;
    }
};