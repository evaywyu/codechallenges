class Solution {
public:
    vector<int> diffWaysToCompute(string input) {
    std::vector<int> output;
    for(int range = 0; range < input.length(); ++range) {
      auto character = input[range];
      if(character == '+' || character == '-' || character == '*'){
        auto a = diffWaysToCompute(input.substr(0,range));
        auto b = diffWaysToCompute(input.substr(range+1));
        for(int m: a){
          for(int n: b){
            if(character == '+'){
              output.push_back(m + n);
            }else if(character == '-'){
              output.push_back(m - n);
            }else if(character == '*'){
              output.push_back(m * n);
            }
          }
        }
      }
    }
    if(output.empty()){
      output.push_back(atoi(input.c_str()));
    }
    return output;
  }
};
