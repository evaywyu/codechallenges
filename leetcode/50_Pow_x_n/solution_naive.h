class Solution {
public:
    double myPow(double x, int n) {
        if(x==0)return 0;
        if(n==0)return 1;

        double result = 1;
        for(int i = 1; i <= n; ++i){
        	result *= x;
        }

        return result;
    }
};