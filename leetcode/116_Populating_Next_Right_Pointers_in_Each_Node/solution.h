/**
 * Definition for binary tree with next pointer.
 * struct TreeLinkNode {
 *  int val;
 *  TreeLinkNode *left, *right, *next;
 *  TreeLinkNode(int x) : val(x), left(NULL), right(NULL), next(NULL) {}
 * };
 */
class Solution {
public:
	/**
	 The idea is to walk using two pointers, to move on the levels
	 Top will point to the previous level, and p will point to the actual
	 Node in the current level
	 */
    void connect(TreeLinkNode *root) {
        TreeLinkNode* top = root;
        while(top != nullptr){
        	TreeLinkNode* firstOnLevel = nullptr;
        	TreeLinkNode* p = nullptr;

        	while(top != nullptr){
        		if(firstOnLevel == nullptr){
        			firstOnLevel = top->left;
        		}

        		if(top->left != nullptr){
        			if(p != nullptr){
        				p->next = top->left;
        			}
        			p = top->left;
        		}

        		if(top->right != nullptr){
        			if(p != nullptr){
        				p->next = top->right;
        			}
        			p = top->right;
        		}
        		top = top->next;
        	}
        	top = firstOnLevel;
        }
    }
};