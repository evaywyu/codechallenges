Given a sorted array of integers, find the starting and ending position of a given
target value. Your algorithm’s runtime complexity must be in the order of O(log n). If
the target is not found in the array, return [-1, -1]. For example, given [5, 7, 7, 8, 8, 10]
and target value 8, return [3, 4].


--- 

Binary search, talk about lower_bound, upper_bound, this may work for a normal case
in other case, like the example

make a binary search of the values with one small difference from left and right of the interval
on left, we need to see an increase from i to i-1
on right, we need to see an increase from i to i+1 (or end)
