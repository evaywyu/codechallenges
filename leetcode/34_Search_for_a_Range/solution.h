class Solution {
public:
  int search(vector<int>& nums, int target, int st, int ed, bool left) {
    if (st>ed) {
      return -1;
    }
    else {
      int mid = st + (ed - st) / 2;
      int n = nums.size();
      if (nums[mid] == target) {
        if (left) {
          if (mid == 0 || nums[mid - 1]<nums[mid]) { return mid; }
          else { return search(nums, target, st, mid - 1, left); }
        }
        if (!left) {
          if (mid == n - 1 || nums[mid + 1]>nums[mid]) { return mid; }
          else { return search(nums, target, mid + 1, ed, left); }
        }
      }
      if (nums[mid] < target) {
        return search(nums, target, mid + 1, ed, left);
      }
      if (nums[mid] > target) {
        return search(nums, target, st, mid - 1, left);
      }
    }
  }

  vector<int> searchRange(vector<int>& nums, int target) {
    vector<int> res(2, -1);
    res[0] = search(nums, target, 0, nums.size() - 1, true);
    res[1] = search(nums, target, 0, nums.size() - 1, false);
    return res;
  }
};