class Solution {
public:
    int rob(vector<int>& nums) {
        if(nums.empty()){ return 0; }
        std::vector<int> amountToRob(nums.size()+1,0);
        amountToRob[1] = nums[0];
        for(int i = 2; i < nums.size() + 1 ; ++i){
            amountToRob[i] = std::max(amountToRob[i-1],amountToRob[i-2] + nums[i-1]);
        }
        return amountToRob[nums.size()];
    }
};