class Solution {
public:
    vector<string> summaryRanges(vector<int>& nums) {
        std::vector<std::string> result;
        if(nums.size()==0){
            return result;
        }
        if(nums.size()==1){
            std::stringstream sb;
            sb << nums[0];
            result.push_back(sb.str());
        }

        // previos element we had
        int pre = nums[0];
        int first = pre; // first element of each range

        for(int i=1; i<nums.size(); i++){
            if(nums[i]==pre+1){
                // If we have ended the array
                if(i==nums.size()-1){
                    std::stringstream sb;
                    sb << first << "->" << nums[i];
                    result.push_back(sb.str());
                }
            }else{
                if(first == pre){
                    std::stringstream sb;
                    sb << first;
                    result.push_back(sb.str());
                }else{
                    std::stringstream sb;
                    sb << first << "->" << pre;
                    result.push_back(sb.str());
                }
                // In case we are at the end and no interval
                if(i==nums.size()-1){
                    std::stringstream sb;
                    sb << nums[i];
                    result.push_back(sb.str());
                }
                first = nums[i];
            }
            pre = nums[i];
        }
        return result;
    }
};
