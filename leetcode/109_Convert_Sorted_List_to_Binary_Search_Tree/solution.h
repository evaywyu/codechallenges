/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
	ListNode* h = nullptr;
public:
    TreeNode* sortedListToBST(ListNode* head) {
        if(head == nullptr){
        	return nullptr;
        }
        h = head;
        int len = getLength(head);
        return sortedListToBST(0,len-1);
    }
    int getLength(ListNode* head){
    	int len = 0;
    	ListNode* p = head;
    	while(p != nullptr){
    		len++;
    		p = p->next;
    	}
    	return len;
    }
    TreeNode* sortedListToBST(int start, int end){
    	if(start > end){
    		return nullptr;
    	}

    	int mid = (start + end) / 2;
    	TreeNode* left = sortedListToBST(start, mid - 1);
    	TreeNode* root = new TreeNode(h->val);
    	h = h->next;
    	TreeNode* right = sortedListToBST(mid + 1, end);
    	root->left = left;
    	root->right = right;
    	return root;
    }
};