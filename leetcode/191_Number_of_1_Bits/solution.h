class Solution {
public:
    int hammingWeight(uint32_t n) {
        int numOfOnes = 0;
        for(int i = 0; i < 32; ++i) {
            numOfOnes += n & 0x1;
            n = n >> 1;
        }
        return numOfOnes;
    }
};
