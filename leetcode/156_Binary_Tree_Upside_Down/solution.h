TreeNode* UpsideDownBinaryTree(TreeNode* root){
	TreeNode* p = root, parent = nullptr, parentRight = nullptr;
	while(p != nullptr){
		TreeNode* left = p->left;
		p->left = parentRight;
		parentRight = p->right;
		p->right = parent;
		parent = p;
		p = left;
	}
	return parent;
}