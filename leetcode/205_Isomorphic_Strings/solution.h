class Solution {
public:
    bool isIsomorphic(string s, string t) {
        std::unordered_map<char,char> auxmap(s.size()+t.size());
        for(int i = 0,j = 'a'; i < s.length(); ++i){
            auto position = auxmap.find(s[i]);
            if(position == auxmap.end()){
                auxmap[s[i]] = j;
                s[i] = j;
                j++;
            }else{
                s[i] = position->second;
            }
        }
        auxmap.clear();
        for(int i = 0,j = 'a'; i < t.length(); ++i){
            auto position = auxmap.find(t[i]);
            if(position == auxmap.end()){
                auxmap[t[i]] = j;
                t[i] = j;
                j++;
            }else{
                t[i] = position->second;
            }
        }
        
        if(s == t){ return true; }
        return false;
    }
};