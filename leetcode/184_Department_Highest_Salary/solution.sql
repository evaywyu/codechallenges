SELECT 
    d.Name as Department, 
    e.Name as Employee,
    e.Salary
FROM Employee e
JOIN Department d
  ON e.DepartmentID = d.Id
WHERE Salary = (SELECT MAX(e2.Salary) 
                FROM Employee e2 
                WHERE e2.DepartmentId = e.DepartmentId)
