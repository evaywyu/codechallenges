class Solution{
public:

  void mergeIslands(vector<vector<char>>& grid, int i, int j){
    if(i < 0 || j < 0 || i > grid.size() - 1 || j > grid[0].size() - 1){
      return;
    }
    // Water or visited
    if(grid[i][j] != '1') return;

    grid[i][j] = '2';
    mergeIslands(grid, i - 1, j);
    mergeIslands(grid, i + 1, j);
    mergeIslands(grid, i, j - 1);
    mergeIslands(grid, i, j + 1);
  }
  int numIslands(vector<vector<char>>& grid){
    if(grid.empty() || grid[0].empty()) return 0;
    int retVal = 0;
    for(int i = 0; i < grid.size(); ++i){
      for(int j = 0; j < grid[0].size(); ++j){
        if(grid[i][j] == '1'){
          ++retVal;
          mergeIslands(grid,i,j);
        }
      }
    }
    return retVal;
  }
};
