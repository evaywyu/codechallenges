Given a linked list, swap every two adjacent nodes and return its head.

For example,
Given 1->2->3->4, you should return the list as 2->1->4->3.

Your algorithm should use only constant space. You may not modify the values in the list, only nodes itself can be changed.

---

use the fast/slow approach with a fake header


    1-2-3-4
  /
F
P

Now until we have TWO elements 

     1-2-3-4
    /
  F
  P  
  S

1º 
  ListNode* t1 = p;
  p = p->next;
  t1->next = p->next;

  	1-2-3-4
   /  |
  F   |			// At this point we actually have 2-1-3-4
  S P |
   \-/
2º

 ListNode* t2 = p->next->next;
 p->next->next = p;
 p->next = t2;

     1-2-3-4..5
    /  |
   F   |
   S   | F    // At his point we actual have 2-1-4-3..5
    \-/  P
