Given two words word1 and word2, find the minimum number of steps required to convert word1 to word2. (each operation is counted as 1 step.)

You have the following 3 operations permitted on a word:

a) Insert a character
b) Delete a character
c) Replace a character

-----------


Let dp[i][j] stands for the edit distance between two strings with length i and j, i.e., word1[0,...,i-1] and word2[0,...,j-1]. There is a relation between dp[i][j] and dp[i-1][j-1]. Let’s say we transform from one string to another. The ﬁrst string has length i and it’s last character is "x"; the second string has length j and its last character is "y". The following diagram shows the relation.


if x == y, then dp[i][j] == dp[i-1][j-1] 
if x != y, and we insert y for word1, then dp[i][j] = dp[i][j-1] + 1
if x != y, and we delete x for word1, then dp[i][j] = dp[i-1][j] + 1 
if x != y, and we replace x with y for word1, then dp[i][j] = dp[i-1][j-1] + 1 
When x!=y, dp[i][j] is the min of the three situations.

Initial condition: dp[i][0] = i, dp[0][j] = j
