class Solution {
public:
  bool isPowerOfTwo(int n) {
    /**The first half of the expression ensures that x is a positive integer.
    The second half of the expression, (x & (~x + 1)) == x, is true only when x
    is a power of two. It compares x with its two’s complement. The two’s
    complement of x is computed with ~x + 1, which inverts the bits of x and
    adds 1 (~x + 1 is equivalent to -x, but negation is technically illegal for
    an unsigned integer).

    Let n be the position of the leftmost 1 bit if x. If x is a power of two,
    its lone 1 bit is in position n. This means ~x has a 0 in position n and 1s
    everywhere else. When 1 is added to ~x, all positions below n become 0 and
    the 0 at position n becomes 1. In other words, the carry propagates all the
    way to position n. So what happens is this: negating x inverts all its bits,
    but adding 1 inverts them back, from position n on down. So, (x & (~x + 1))
    == x is true.

    x	            ~x	      ~x + 1	x & (~x + 1)
    00000001	11111110	11111111	00000001
    00000100	11111011	11111100	00000100

    */
    unsigned long x = static_cast<unsigned long int>(n);
    return ((x != 0) && ((x & (~x + 1)) == x));
  }
};
