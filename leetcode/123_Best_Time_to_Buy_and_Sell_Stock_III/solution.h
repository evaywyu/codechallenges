class Solution {
public:
    int maxProfit(vector<int>& prices) {
        if(prices.size() < 2){
        	return 0;
        }

        std::vector<int> left(prices.size());
        std::vector<int> right(prices.size());

        left[0] = 0;
        int minimum = prices[0];
        for(int i = 1; i < prices.size(); ++i){
        	minimum = std::min(minimum,prices[i]);
        	left[i] = std::max(left[i-1],prices[i]-minimum);
        }

        right[prices.size()-1] = 0;
        int maximum = prices[prices.size() -1];
        for(int i = prices.size() - 2; i >= 0; --i){
        	maximum = std::max(maximum, prices[i]);
        	right[i] = std::max(right[i+1],maximum - prices[i]);
        }

        int profit = 0;
        for(int i = 0; i < prices.size(); ++i){
        	profit = std::max(profit, left[i] + right[i]);
        }
        return profit;
    }
};