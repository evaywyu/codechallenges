class Solution {
public:
    int computeArea(int A, int B, int C, int D, int E, int F, int G, int H) {
        int firstArea = (C-A) * (D-B);
        int secondArea = (G-E) * (H - F);
        int thirdArea = 0;
        
        // Check if overlap
        if (A < G && C > E &&
            B < H && D > F) {
             int x_overlap = std::max(0, std::min(C,G) - std::max(A,E));
             int y_overlap = std::max(0, std::min(D,H) - std::max(B,F));
             thirdArea = x_overlap * y_overlap;
        }
        return abs(firstArea) + abs(secondArea) - thirdArea;
    }
};