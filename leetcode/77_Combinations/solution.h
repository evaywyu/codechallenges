class Solution {
public:
	void dfs(int n, int k, int start, vector<int> &temp, vector<vector<int>>& result){
		if(temp.size() == k){
			result.push_back(temp);
			return;
		}
		for(int i = start; i <= n; ++i){
			temp.push_back(i);
			dfs(n,k,i+1,temp,result);
			temp.pop_back();
		}
	}
    vector<vector<int>> combine(int n, int k) {
        vector<vector<int>> result;
        if(n<=0||k<=0){
        	return result;
        }
        vector<int> temp;
        dfs(n,k,1,temp,result); // 1..n
        return result;
    }
};