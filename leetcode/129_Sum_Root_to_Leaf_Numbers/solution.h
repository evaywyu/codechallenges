/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
	int dfs(TreeNode* node, int num, int sum){
		if(node == nullptr){
			return sum;
		}
		num = num * 10 + node->val;
		if(node->left == nullptr && node->right == nullptr){
			sum += num;
			return sum;
		}

		sum = dfs(node->left, num, sum) + dfs(node->right, num, sum);
		return sum;

	}
    int sumNumbers(TreeNode* root) {
        if(root == nullptr){
        	return 0;
        }
        return dfs(root,0,0);
    }
};