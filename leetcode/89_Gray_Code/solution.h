class Solution {
public:
    unsigned int binary2gray(unsigned int bi){
        return (bi>>1)^bi;
    }
    vector<int> grayCode(int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        vector<int> res;
        int total = pow(2,n);
        for (unsigned int i=0;i<total;i++){
            res.push_back(binary2gray(i));
        }
        return res;
    }
};