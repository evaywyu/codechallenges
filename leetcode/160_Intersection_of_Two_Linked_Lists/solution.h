/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:

    ListNode *getIntersectionNode(ListNode *headA, ListNode *headB) {
        if(headA == nullptr || headB == nullptr) return nullptr;
        int lengthA = 0;
        int lengthB = 0;
        ListNode* aux = headA;
        while(aux!=nullptr){
            lengthA++;
            aux=aux->next;
        }
        aux = headB;
        while(aux!=nullptr){
            lengthB++;
            aux=aux->next;
        }
        ListNode* listA = headA;
        ListNode* listB = headB;
        // Now we need to advance one of them to the diff
        int dif = abs(lengthA - lengthB);
        if(lengthA > lengthB){
            while(dif>0){
                dif--;
                listA = listA->next;
            }
        }else if(lengthB > lengthA){
            while(dif>0){
                dif--;
                listB = listB->next;
            }
        }
        while(listA != nullptr && listB != nullptr ){
            if(listA == listB){
                return listA;
            }
            listA = listA->next;
            listB = listB->next;
        }
        return nullptr;
    }
};