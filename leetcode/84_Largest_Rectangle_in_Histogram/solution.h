class Solution {
public:
    int largestRectangleArea(vector<int>& height) {
        if(height.empty()){
            return 0;
        }
        std::stack<int> hs;
        
        int maximum = 0;
        int i = 0;
        while(i < height.size()){
            if(hs.empty() || height[i] >= height[hs.top()]){
                hs.push(i);
                ++i;
            }else{
                int topV = hs.top(); hs.pop();
                int hei  = height[topV];
                int width = hs.empty() ? i : i - hs.top() - 1;
                maximum = std::max(hei * width, maximum);
            }
        }
        while(!hs.empty()){
            int topV = hs.top(); hs.pop();
            int hei  = height[topV];
            int width = hs.empty() ? i : i - hs.top() - 1;
            maximum = std::max(hei * width, maximum);
        }
        return maximum;
    }
};