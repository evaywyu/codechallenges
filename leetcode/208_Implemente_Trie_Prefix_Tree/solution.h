class TrieNode {
public:
    // Initialize your data structure here.
    TrieNode() {
    }
    TrieNode(char car) 
      : c(car){
    }
    char c;
    bool isLeaf = false;
    unordered_map<char,TrieNode*> children;
};

class Trie {
public:
    Trie() {
        root = new TrieNode();
    }

    // Inserts a word into the trie.
    void insert(string word) {
        TrieNode* actualNode = root;
        
        for(int i = 0; i < word.length(); ++i){
            char c = word[i];
            
            auto contains = actualNode->children.find(c);
            if(contains != actualNode->children.end()){
                actualNode = contains->second;
            }else{
                TrieNode* t = new TrieNode(c);
                actualNode->children[c] = t;
                actualNode = t;
            }
            if( i == word.length() - 1){
                actualNode->isLeaf = true;
            }
        }
    }

    // Returns if the word is in the trie.
    bool search(string word) {
        TrieNode* t = searchNode(word);
        if( t != nullptr && t->isLeaf){
            return true;
        }
        return false;
    }

    // Returns if there is any word in the trie
    // that starts with the given prefix.
    bool startsWith(string prefix) {
        if(searchNode(prefix) != nullptr){
            return true;
        }
        return false;
    }

    TrieNode* searchNode(string& str){
        TrieNode* actualNode = root;
        TrieNode* retVal = nullptr;
        for(int i = 0; i < str.length(); ++i){
            char c = str[i];
            auto contains = actualNode->children.find(c);
            if(contains != actualNode->children.end()){
                actualNode = contains->second;
                retVal = actualNode;
            }else{
                return nullptr;
            }
        }
        return retVal;
    }
private:
    TrieNode* root;
};

// Your Trie object will be instantiated and called as such:
// Trie trie;
// trie.insert("somestring");
// trie.search("key");