class Solution:
    """ The Question only requires inorderTraveral. But for kicks, here 
        are all the orderlies.

        inorder: Values are ordered leftmost first.
                 i.e left - parent - right, bottom up
        preorder: values are ordered as the are visited. 
                  ie, parent then left then right.
        postorder: values are ordered children before parent. 
                   ie, left, right, parent

        All solutions are recursive.
    """
    def inorder(self, node: TreeNode, results: List[TreeNode]) -> None:
        if node:
            self.inorder(node.left, results)
            results.append(node.val)
            self.inorder(node.right, results)

    def preorder(self, node: TreeNode, results: List[TreeNode]) -> None:
        if node:
            print(node.val, end=", ")
            results.append(node.val)
            self.preorder(node.left, results)
            self.preorder(node.right, results)

    def postorder(self, node: TreeNode, results: List[TreeNode]) -> None:
        if node:
            self.postorder(node.left, results)
            self.postorder(node.right, results)
            results.append(node.val)

    def inorderTraversal(self, root: TreeNode) -> List[int]:
        inorder_values = []
        self.inorder(root, inorder_values)
        return inorder_values
