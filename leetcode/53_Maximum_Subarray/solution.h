class Solution {
public:
    int maxSubArray(vector<int>& nums) {
        int newsum = nums[0];
        int maximum = nums[0];

        for(int i = 1; i < nums.size(); ++i){
        	newsum = std::max(newsum + nums[i], nums[i]);
        	maximum = std::max(maximum,newsum);
        }
        return maximum;
    }
};