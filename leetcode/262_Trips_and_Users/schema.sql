CREATE TYPE trips_status
    AS ENUM ('completed',
             'cancelled_by_driver',
             'cancelled_by_client');

CREATE TYPE users_is_banned
    AS ENUM ('Yes',
             'No');

CREATE TYPE users_role
    AS ENUM ('client',
             'driver',
             'partner');

CREATE TABLE Trips (Id int,
                    Client_Id int,
                    Driver_Id int,
                    City_Id int,
                    Status trips_status,
                    Request_at date);

INSERT INTO Trips
VALUES (1,1,10,1,'completed','2013-10-01'),
       (2,2,11,1,'cancelled_by_driver','2013-10-01'),
       (3,3,12,6,'completed','2013-10-01'),
       (4,4,13,6,'cancelled_by_client','2013-10-01'),
       (5,1,10,1,'completed','2013-10-02'),
       (6,2,11,6,'completed','2013-10-02'),
       (7,3,12,6,'completed','2013-10-02'),
       (8,2,12,12,'completed','2013-10-03'),
       (9,3,10,12,'completed','2013-10-03'),
       (0,4,13,12,'cancelled_by_driver','2013-10-03');

CREATE TABLE Users (Id int,
                    Banned users_is_banned,
                    Role users_role);

INSERT INTO Users
VALUES (1 ,'No', 'client'),
       (2 ,'Yes', 'client'),
       (3 ,'No', 'client'),
       (4 ,'No', 'client'),
       (10,'No', 'driver'),
       (11,'No', 'driver'),
       (12,'No', 'driver'),
       (13,'No', 'driver');
