select 
	   request_at as Day,
	   round((count(*) filter (where status <> 'completed') * 1.0 / count(*)), 2) as "Cancellation Rate"
from Trips trips
inner join (select id 
       from users
       where banned = 'No' 
       and role = 'client') NotBannedClient 
on NotBannedClient.id = trips.Client_Id 
inner join ( select id 
       from users
       where banned='No'
       and role ='driver') NotBannedDriver
on NotBannedDriver.id = trips.Driver_Id 
where Request_at 
	between '2013-10-01' and '2013-10-03'
group by request_at;
