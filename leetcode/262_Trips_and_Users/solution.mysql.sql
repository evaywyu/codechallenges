# Write your MySQL query statement below
select request_at as Day,
       ROUND((count(CASE WHEN Status <> 'completed' THEN 1 ELSE NULL END) * 1.0 / count(status)),2) as "Cancellation Rate"
from Trips trips
 join (select Users_Id  
       from users
       where banned = 'No' 
       and role = 'client') NotBannedClient 
on NotBannedClient.Users_Id  = trips.Client_Id 
 join ( select Users_Id  
       from users
       where banned='No'
       and role ='driver') NotBannedDriver
on NotBannedDriver.Users_Id  = trips.Driver_Id 
where Request_at 
	between '2013-10-01' and '2013-10-03'
group by request_at;

