class Solution {
public:
    bool noswap(int k, int i, const vector<int>& num){
        for (int j=k;j<i;j++){
            if (num[j]==num[i]){
                return true;
            }
        }
        return false;
    }
	void permute(vector<int> nums, int start, int end, vector<vector<int>> & result){
		if(start == end){
			result.push_back(nums);
		}else{
			for(int j = start; j <= end; ++j){
			    if(noswap(start,j,nums)){continue;}
				std::swap(nums[start],nums[j]);
				permute(nums,start+1,end,result);
				std::swap(nums[start],nums[j]);
			}
		}
	}
    vector<vector<int>> permuteUnique(vector<int>& nums) {
        vector<vector<int>> result;
        permute(nums,0, nums.size()-1, result);
        return result;
    }
};