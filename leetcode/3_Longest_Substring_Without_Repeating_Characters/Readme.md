Given a string, find the length of the longest substring without repeating characters.
For example, the longest substring without repeating letters for "abcabcbb" is "abc", which the length is 3.
For "bbbbb" the longest substring is "b", with the length of 1.


---


We must keep a string on a list/vector/deque with the biggest word, if we find a character on the list,
we just need to erase that area and continue