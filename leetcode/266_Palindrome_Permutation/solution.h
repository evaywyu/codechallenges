class Solution {
public:
    bool canPermutePalindrome(string s) {
        unordered_map<char, int> hash;
        int odd_cnt = 0;
        for(char c:s){
            hash[c]++;
            if(hash[c]%2==1){
                odd_cnt++;
            }else{
                odd_cnt--;
            }
        }
        return odd_cnt<=1;
    }
};

public class Solution {
    public boolean canPermutePalindrome(String s) {
        if (s == null || s.length() <= 1) {
            return true;
        }
         
        Map<Character, Integer> map = new HashMap<Character, Integer>();
         
        for (int i = 0; i < s.length(); i++) {
            char letter = s.charAt(i);
             
            if (map.containsKey(letter)) {
                int count = map.get(letter) + 1;
                map.put(letter, count);
            } else {
                map.put(letter, 1);
            }
        }
         
        int tolerance = 0;
        Iterator it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
             
            if ((int) pair.getValue() % 2 != 0) {
                tolerance++;
            }
        }
         
        if (s.length() % 2 == 0) {
            return tolerance == 0;
        } else {
            return tolerance == 1;
        }
    }
};



