/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<int> rightSideView(TreeNode* root) {
    	std::vector<int> result;
    	if(root == nullptr){
    		return result;
    	}
        std::queue<TreeNode*> q;
        q.push(root);
        while(!q.empty()){
        	int queueSize = q.size();
        	for( int i = 0; i < queueSize; ++i){
        		TreeNode* top = q.front();
        		q.pop();
        		if(i == 0){
        			result.push_back(top->val);
        		}
        		if(top->right != nullptr){
        			q.push(top->right);
        		}
        		if(top->left != nullptr){
        			q.push(top->left);
        		}
        	}
        }
        return result;
    }
};