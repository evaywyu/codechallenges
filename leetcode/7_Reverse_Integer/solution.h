#include <limits>

class Solution {
public:
    int reverse(int x) {
        if(x == 0) return 0;
        int64_t x_ = x;
        int64_t maxUnits = 1;
        while(x_ != 0){
            x_ /= 10;
            maxUnits *= 10;
        }
        x_ = x;
        int64_t result = 0;
        int64_t currentUnit = maxUnits/10;
        while(x_ != 0){
            int64_t dec = x_ % 10;
            x_ /= 10;
            result += (currentUnit) * dec;
            currentUnit/=10;
        }
        if(result > std::numeric_limits<int>::max() ||
       result < std::numeric_limits<int>::min() ) return 0;

        return result;
    }
};
