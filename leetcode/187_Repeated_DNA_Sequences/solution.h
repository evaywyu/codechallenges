class Solution {
public:
    long str2long(string s){
        long res = 0;
        for (int i=0;i<10;i++){
            if (s[i] == 'A'){res = res*10 + 1;}
            if (s[i] == 'T'){res = res*10 + 2;}
            if (s[i] == 'C'){res = res*10 + 3;}
            if (s[i] == 'G'){res = res*10 + 4;}
        }
        return res;
    }
    
    string long2str(long s){
        string res = "";
        for (int i=0;i<10;i++){
            int d = s%10;
            if (d == 1) {res= "A" + res;}
            if (d == 2) {res= "T" + res;}
            if (d == 3) {res= "C" + res;}
            if (d == 4) {res= "G" + res;}
            s = s /10;
        }
        return res;
    }

    vector<string> findRepeatedDnaSequences(string s) {
        if (s.size() < 10) return std::vector<std::string>();
        std::vector<std::string> result;
        std::map<long, int> occ;
        for (int i = 0; i < s.size() - 9; ++i) {
          long val = str2long(s.substr(i, 10));
          auto pos = occ.find(val);
          if (pos == occ.end()) {
            occ.insert(std::make_pair(val, 0));
          }
          else {
            pos->second++;
          }
        }
    
        for (auto& i : occ) {
          if (i.second >= 1) {
            result.push_back(long2str(i.first));
          }
        }
        return result;
    }
};