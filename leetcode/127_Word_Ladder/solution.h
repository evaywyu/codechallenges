class SolutionNode {
public:
  SolutionNode(const std::string& v, int d) : actualWord(v), depth(d) {}
  std::string actualWord;
  int depth;
};

class Solution {
public:
  int ladderLength(string beginWord, string endWord, unordered_set<string>& wordDict) {
    std::queue<SolutionNode> wordQueue;
    wordQueue.push(SolutionNode(beginWord, 1));
    wordDict.insert(endWord); // <<-- Optional??? care for possible trick on interview, dont put it in first instance

    while (!wordQueue.empty()) {
      SolutionNode element = wordQueue.front();
      wordQueue.pop();

      if (element.actualWord == endWord) {
        return element.depth;
      }

      for (size_t i = 0; i < element.actualWord.size(); ++i) {
        for (char c = 'a'; c <= 'z'; ++c) {
          char tmp = element.actualWord[i];
          if (element.actualWord[i] != c) {
            element.actualWord[i] = c;
          }

          auto it = wordDict.find(element.actualWord);
          if (it != wordDict.end()) {
            wordQueue.push(SolutionNode(element.actualWord, element.depth + 1));
            wordDict.erase(it);
          }
          element.actualWord[i] = tmp;
        }
      }
    }
    return 0;
  }
};