-- Is there a more optimized way to do this? Probably. 
SELECT DISTINCT 
    l1.num as ConsecutiveNums 
  FROM logs l1
  JOIN logs l2
  JOIN logs l3
   ON  l3.id - l1.id = 2
   AND L2.ID - L1.ID = 1
   AND l1.num = l2.num
   AND l1.num = l3.num
