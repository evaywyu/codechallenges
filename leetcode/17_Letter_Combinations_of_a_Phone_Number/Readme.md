Given a digit string, return all possible letter combinations that the number could represent.

A mapping of digit to letters (just like on the telephone buttons) is given below.



Input:Digit string "23"
Output: ["ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"].
Note:
Although the above answer is in lexicographical order, your answer could be in any order you want.


-----

Tipical Recusivity problem

Need a map with all the keys and values

for each digit on the input
	get the list of digits on that key
		update the actual string with one digit of the list
		call recursively
		remove that character