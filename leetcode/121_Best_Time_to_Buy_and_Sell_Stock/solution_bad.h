class Solution {
public:
    int maxProfit(vector<int>& prices) {
        if(prices.size() < 2){
        	return 0;
        }
        int profit = std::numeric_limits<int>::min();
        for(int i=0; i < prices.size() - 1; ++i){
        	for(int j = 0; j < prices.size(); ++j){
        		if(profit < prices[j] - prices[i]){
        			profit = prices[j] - prices[i];
        		}
        	}
        }
        return profit;
    }
};