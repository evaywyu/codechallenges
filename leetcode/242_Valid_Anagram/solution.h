class Solution {
public:
  bool isAnagram(string s, string t) {
    std::map<char, int> mapChar;
    if (s.size() != t.size()) {
      return false;
    }
    for (auto car : s) {
      if (mapChar.find(car) == mapChar.end()) {
        mapChar[car] = 1;
      } else {
        mapChar[car] += 1;
      }
    }
    for (auto car : t) {
      if (mapChar.find(car) == mapChar.end()) {
        mapChar[car] = -1;
      } else {
        mapChar[car] -= 1;
      }
    }
    for (auto &entry : mapChar) {
      if (entry.second != 0) {
        return false;
      }
    }
    return true;
  }
};
