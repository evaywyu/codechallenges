class Solution:
    def plusOne(self, digits: List[int]) -> List[int]:
        """ First Solution:
        Iterate backwards as long as there is a carry over > 0.
        Time Complexity: O(n)

        Thoughts: I may be able to further reduce space 
                  complexity if i did not splice the list.
        """
        carry = 0
        if digits[-1] + 1 > 9:
            digits[-1] = 0
            carry = 1
        else:
            digits[-1] += 1
            return digits

        for i, digit in enumerate(digits[:-1][::-1]):
            if digit + carry > 9:
                carry = 1
                digits[-(i+2)] = 0
            else:
                digits[-(i+2)] += carry
                return digits
        return (digits if carry == 0 else [1] + digits)

    def plusOne2(self, digits: List[int]) -> List[int]:
        """ Second Solution.
            The Lazy Cheat-y and more iterations way
            Python join as string, cast to int, + 1, split.
            ...LOL.
            Still, its technically O(n).
        """
        numeric = int(''.join(str(d) for d in digits)) + 1
        return [int(i) for i in str(numeric)]
