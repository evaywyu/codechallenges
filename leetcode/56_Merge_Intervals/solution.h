/**
 * Definition for an interval.
 * struct Interval {
 *     int start;
 *     int end;
 *     Interval() : start(0), end(0) {}
 *     Interval(int s, int e) : start(s), end(e) {}
 * };
 */
class Solution {
public:
    static bool compareOrderedInterval (Interval& i,Interval& j) { 
        return (i.start < j.start); 
    }

    vector<Interval> merge(vector<Interval>& intervals) {
        if(intervals.empty()) return std::vector<Interval>();
        if(intervals.size() == 1) return intervals;
        
        std::vector<Interval> result;
        std::sort(intervals.begin(),intervals.end(),compareOrderedInterval);
        Interval first = intervals[0];
        for(int i = 1; i < intervals.size(); ++i){
            Interval actual = intervals[i];
            if(first.end >= actual.start){
                first = Interval(first.start,std::max(first.end,actual.end));
            }else{
                result.push_back(first);
                first = actual;
            }
        }
        result.push_back(first);
        
        return result;
    }
};