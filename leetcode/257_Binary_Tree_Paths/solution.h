/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
  void onePath(std::vector<int> actualPath, TreeNode *node,
               std::vector<std::vector<int>> &totalPaths) {
    if (node == nullptr) {
      return;
    }
    if (node != nullptr && node->left == nullptr && node->right == nullptr) {
      actualPath.push_back(node->val);
      totalPaths.push_back(actualPath);
    } else {
      actualPath.push_back(node->val);
      onePath(actualPath, node->left, totalPaths);
      onePath(actualPath, node->right, totalPaths);
    }
  }

  std::string pathToString(const std::vector<int> &path) {
    std::stringstream result;
    for (size_t i = 0; i < path.size(); ++i) {
      if (i > 0)
        result << "->";
      result << path[i];
    }
    return result.str();
  }

  vector<string> binaryTreePaths(TreeNode *root) {
    std::vector<std::string> solution;
    if (root == nullptr) {
      return std::vector<std::string>();
    }
    std::vector<std::vector<int>> listOfPaths;
    std::vector<int> actualPath;
    onePath(actualPath, root, listOfPaths);
    for (auto &paths : listOfPaths) {
      solution.push_back(pathToString(paths));
    }
    return solution;
  }
};
