typedef unsigned char UCHAR;
class Codec {
    const static int MAX_CNT = 255;
public:

    // Encodes a list of strings to a single string.
    string encode(vector<string>& strs) {
        string ret;
        for(auto &s : strs)
        {
            int i = 0, len = s.length();
            
            while(i < len)
            {
                UCHAR c = s[i];
                UCHAR cnt = 1;
                while(i < len - 1 && s[i + 1] == c && cnt < (MAX_CNT - 1))
                {
                    i ++; cnt ++;
                }
                ret += UCHAR(cnt);
                ret += UCHAR(c);
                
                i ++;
            }
            ret += UCHAR(MAX_CNT); // 0xFF: end
        }
        return ret;
    }

    // Decodes a single string to a list of strings.
    vector<string> decode(string s) 
    {
        vector<string> ret;

        size_t len = s.length();
        string cur; int inx = 0;
        while(inx < len)
        {
            UCHAR cnt = s[inx];
            if(cnt == UCHAR(MAX_CNT))
            {
                ret.push_back(cur);
                cur = "";
                inx ++;
                continue;
            }
            //
            UCHAR c = s[inx + 1];
            for(UCHAR i = 0; i < cnt; i ++)    cur += c;
            inx += 2;                        
        }
        return ret;
    }
};