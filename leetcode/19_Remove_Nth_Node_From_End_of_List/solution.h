/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* removeNthFromEnd(ListNode* head, int n) {
        if (head == nullptr) return nullptr;

        ListNode* fast = head;
        ListNode* slow = head;
    
        for (int i = 0; i < n; ++i) {
          fast = fast->next;
        }
    
        // If first node
        if (fast == nullptr) {
          head = head->next;
          return head;
        }
        
        // any other node
        while (fast->next != nullptr) {
          fast = fast->next;
          slow = slow->next;
        }
        slow->next = slow->next->next;;
        return head;
    }
};