class Solution:
    def searchInsert(self, nums: List[int], target: int) -> int:
        """They are asking for Binary Search."""
        head = 0
        tail = len(nums)
        while head < tail:
            # check value at mid
            mid = (head + tail) // 2
            if nums[mid] == target:
                return mid
            elif nums[mid] > target:
                tail = mid
            else:
                head = mid + 1
        return head
