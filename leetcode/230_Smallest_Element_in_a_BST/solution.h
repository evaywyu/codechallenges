/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    int kthSmallest(TreeNode* root, int k) {
        stack<TreeNode*> nodes;
        TreeNode* actualNode = root;
        while(!nodes.empty() || actualNode != nullptr){
            if(actualNode != nullptr){
                nodes.push(actualNode);
                actualNode = actualNode->left;
            }else{
                TreeNode* topNode = nodes.top();
                nodes.pop();
                k--;
                if(k == 0){
                    return topNode->val;
                }
                actualNode = topNode->right;
            }
        }
        return 0;
    }
};