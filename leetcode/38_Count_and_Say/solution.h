#include <sstream>

class Solution {
public:
    string countAndSay(int n) {
        if (n <= 0)
    		return nullptr;
     
    	string result = "1";
    	int i = 1;
     
    	while (i < n) {
    		std::stringstream sb;
    		int count = 1;
    		for (int j = 1; j < result.length(); j++) {
    			if (result[j] == result[j - 1]) {
    				count++;
    			} else {
    				sb << count << result[j-1];
    				count = 1;
    			}
    		}
      		sb << count << result[result.length() - 1];
    		result = sb.str();
    		i++;
    	}
     
    	return result;
    }
};