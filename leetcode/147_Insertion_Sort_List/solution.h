/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* insertionSortList(ListNode* head) {
        if(head == nullptr || head->next == nullptr){
            return head;
        }
        
        ListNode* newHead = new ListNode(head->val);
        ListNode* pointer = head->next;
        
        while(pointer != nullptr){
            ListNode* innerPointer = newHead;
            ListNode* next = pointer->next;
            
            if(pointer->val <= newHead->val){
                ListNode* oldHead = newHead;
                newHead = pointer;
                newHead->next = oldHead;
            }else{
                while(innerPointer->next != nullptr){
                    if(pointer->val > innerPointer->val && pointer->val <= innerPointer->next->val){
                        ListNode* oldNext = innerPointer->next;
                        innerPointer->next = pointer;
                        pointer->next = oldNext;
                    }
                    innerPointer = innerPointer->next;
                }
                if(innerPointer->next == nullptr && pointer->val > innerPointer->val){
                    innerPointer->next = pointer;
                    pointer->next = nullptr;
                }
            }
            pointer = next;
        }
        return newHead;
    }
};