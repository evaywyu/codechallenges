Given an unsorted array, find the maximum difference between the successive elements in its sorted form.

Try to solve it in linear time/space.

Return 0 if the array contains less than 2 elements.

You may assume all elements in the array are non-negative integers and fit in the 32-bit signed integer range.

Credits:
Special thanks to @porker2008 for adding this problem and creating all test cases.

------


We can use a bucket-sort like algorithm to solve this problem in time of O(n) and space O(n). 
The basic idea is to project each element of the array to an array of buckets. Each bucket tracks the maximum and minimum elements. Finally, scanning the bucket list, we can get the maximum gap.

From: interval * (num[i] - min) = 0 and interval * (max -num[i]) = n 
interval = num.length / (max - min)
