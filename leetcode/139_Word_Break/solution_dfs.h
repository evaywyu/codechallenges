class Solution{
public:
	bool wordBreak(string& s, unordered_set<string>& dict, int start){
		if(start == s.length()){
			return true;
		}
		auto it = dict.begin();
		for(;it!=dict.end();++it){
			int len = (*it).length();
			int end = start + len;
			if(end > s.length()){
				continue;
			}
			if(s.substr(start,end) == *it){
				if(wordBreak(s,dict,start+len)){
					return true;
				}
			}
		}
		return false;
	}
	bool wordBreak(string s, unordered_set<string>& dict){
		return wordBreak(s,dict,0);
	}
};