SELECT r1.id 
  FROM weather r1
  JOIN weather r2 
    ON r1.id = r2.id
 WHERE r1.recordDate - INTERVAL 1 DAY = r2.recordDate     
   AND r1.temperature > r2.temperature
