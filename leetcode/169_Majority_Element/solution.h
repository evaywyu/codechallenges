class Solution {
public:

    int majorityElement(vector<int>& nums) {
        if(nums.size() == 1){
            return nums[0];
        }
        int majorityNum = nums.size() / 2;
        std::unordered_map<int,int> majority;
        for(int i = 0; i < nums.size(); ++i){
            auto position = majority.find(nums[i]);
            if(position != majority.end()){
                position->second++;
                if(position->second > majorityNum){
                    return position->first;
                }
            }else{
                majority.insert(std::make_pair(nums[i],1));
            }
        }
    }
};