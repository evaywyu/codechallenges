# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    """ While loop solution. Non Recursive.
    
    Note: Variable 'head' keeps track of the 
    memory address to the beginning of the 
    linked list.
    """
    def mergeTwoLists(self, l1: ListNode, l2: ListNode) -> ListNode:
        head = current = ListNode(0)
        while l1 and l2:
            if l1.val <= l2.val:
                current.next = l1
                l1 = l1.next
            else:
                current.next = l2
                l2 = l2.next
            current = current.next
        current.next = l1 or l2
        return head.next
