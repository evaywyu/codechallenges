/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* mergeTwoLists(ListNode* l1, ListNode* l2) {
        if(l1 == nullptr && l2 == nullptr) return nullptr;
        if(l1 == nullptr) return l2;
        if(l2 == nullptr) return l1;
        
        ListNode* firstNode;
        ListNode* backNode;
        if(l1->val < l2->val){
            firstNode = new ListNode(l1->val);
            l1 = l1->next;
            backNode = firstNode;
        }else{
            firstNode = new ListNode(l2->val);
            l2 = l2->next;
            backNode = firstNode;
        }
        while(l1 != nullptr && l2 != nullptr){
            if(l1->val < l2->val){
                ListNode* tmp = new ListNode(l1->val);
                backNode->next = tmp;
                backNode = backNode->next;
                l1 = l1->next;
            }else{
                ListNode* tmp = new ListNode(l2->val);
                backNode->next = tmp;
                backNode = backNode->next;
                l2 = l2->next;
            }
        }
        while(l1 != nullptr){
            ListNode* tmp = new ListNode(l1->val);
            backNode->next = tmp;
            backNode = backNode->next;
            l1 = l1->next;
        }
        while(l2 != nullptr){
            ListNode* tmp = new ListNode(l2->val);
            backNode->next = tmp;
            backNode = backNode->next;
            l2 = l2->next;
        }
        return firstNode;
    }
};